<?php

namespace App\Models;

use App\Models\Model;

class ItemFeatureDetail extends Model
{
    /**
     * Get the Brend that owns the Model.
     */
    public function item_feature()
    {
        return $this->belongsTo('App\Models\ItemFeature');
    }
    /**
     * Get the Brend that owns the Model.
     */
    public function item_detail()
    {
        return $this->belongsTo('App\Models\ItemDetail');
    }
}
