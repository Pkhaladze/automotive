<?php

namespace App\Models;

use App\Models\Model;

class ItemModel extends Model
{
    /**
     * Get the Brend that owns the Model.
     */
    public function item_brend()
    {
        return $this->belongsTo('App\Models\ItemBrend');
    }
    /**
     * Get the comments for the blog post.
     */
    public function item_details()
    {
        return $this->hasMany('App\Models\ItemDetail');
    }
}
