<?php

namespace App\Models;

use App\Models\Model;

class ItemTechnical extends Model
{
    /**
     * Get the comments for the blog post.
     */
    public function item_technical_catalog()
    {
        return $this->belongsTo('App\Models\ItemTechnicalCatalog');
    }
    /**
     * Get the comments for the blog post.
     */
    public function Item_technical_details()
    {
        return $this->hasMany('App\Models\ItemTechnicalDetail');
    }
}
