<?php

namespace App\Models;

use App\Models\Model;

class ItemFeature extends Model
{
    /**
     * Get the comments for the blog post.
     */
    public function Item_feature_details()
    {
        return $this->hasMany('App\Models\ItemFeatureDetail');
    }
}
