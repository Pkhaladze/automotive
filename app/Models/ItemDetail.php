<?php

namespace App\Models;

use App\Models\Model;

class ItemDetail extends Model
{
    /**
     * Get the comments for the blog post.
     */
    public function item_brend()
    {
        return $this->belongsTo('App\Models\ItemBrend');
    }
    /**
     * Get the comments for the blog post.
     */
    public function item_model()
    {
        return $this->belongsTo('App\Models\ItemModel');
    }
    /**
     * Get the comments for the blog post.
     */
    public function item_body_style()
    {
        return $this->belongsTo('App\Models\ItemBodyStyle');
    }
    /**
     * Get the comments for the blog post.
     */
    public function Item_technical_details()
    {
        return $this->hasMany('App\Models\ItemTechnicalDetail');
    }
    /**
     * Get the comments for the blog post.
     */
    public function  Item_feature_details()
    {
        return $this->hasMany('App\Models\ItemFeatureDetail');
    }
}
