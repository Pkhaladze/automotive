<?php

namespace App\Models;

use App\Models\Model;

class ItemTechnicalDetail extends Model
{
    /**
     * Get the comments for the blog post.
     */
    public function item_technical_catalog()
    {
        return $this->belongsTo('App\Models\ItemTechnicalCatalog');
    }
    /**
     * Get the comments for the blog post.
     */
    public function item_technical()
    {
        return $this->belongsTo('App\Models\ItemTechnical');
    }
    /**
     * Get the comments for the blog post.
     */
    public function item_detail()
    {
        return $this->belongsTo('App\Models\ItemDetail');
    }
}
