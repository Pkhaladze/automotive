<?php

namespace App\Models;

use App\Models\Model;

class ItemTechnicalCatalog extends Model
{
    /**
     * Get the comments for the blog post.
     */
    public function item_technicals()
    {
        return $this->hasMany('App\Models\ItemTechnical');
    }
    /**
     * Get the comments for the blog post.
     */
    public function Item_technical_details()
    {
        return $this->hasMany('App\Models\ItemTechnicalDetail');
    }
}
