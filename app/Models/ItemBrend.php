<?php

namespace App\Models;

use App\Models\Model;

class ItemBrend extends Model
{
    /**
     * Get the comments for the blog post.
     */
    public function item_models()
    {
        return $this->hasMany('App\Models\ItemModel');
    }
    /**
     * Get the comments for the blog post.
     */
    public function item_details()
    {
        return $this->hasMany('App\Models\ItemDetail');
    }
}
