<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ItemBrend;
use App\Models\ItemModel;
use App\Models\ItemBodyStyle;
use App\Models\ItemFeature;
use App\Models\ItemTechnical;
use App\Models\ItemDetail;
use App\Models\ItemFeatureDetail;
use App\Models\ItemTechnicalDetail;
use App\Models\Photo;
use DB;
use Storage;
use Validator;

class AdminItemController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $details = ItemDetail::all();
        return view('admin.inventory.index',  compact('details'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brands = ItemBrend::all();
        $models = ItemModel::all();
        $body_styles = ItemBodyStyle::all();
        $features = ItemFeature::all();
        $technicals = ItemTechnical::all();
        return view('admin.inventory.create_edit', compact('brands', 'models', 'body_styles', 'features', 'technicals'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*dd($request);*/
         $rules = [
            'item_brend_id' => 'required',
            'item_model_id' => 'required',
            'item_body_style_id' => 'required',
            'condition' => 'required',
            'year' => 'required',
        ];


        try {
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return redirect()->back()
                    ->withInput($request->all())
                    ->withErrors($validator);
            }
            DB::beginTransaction();
            $Item_detail = new ItemDetail;
            $Item_detail->item_brend_id = $request->item_brend_id;
            $Item_detail->item_model_id = $request->item_model_id;
            $Item_detail->item_body_style_id = $request->item_body_style_id;
            $Item_detail->condition = $request->condition;
            $Item_detail->price = $request->price;
            $Item_detail->location = $request->location;
            $Item_detail->year = $request->year;
            $Item_detail->overview = $request->overview;
            $Item_detail->comment = $request->comment;
            $Item_detail->save();


            for ($i=1; $i <= 40; $i++) {
                $item_feature_detail = new ItemFeatureDetail;
                if (!$request->$i) {
                    $item_feature_detail->Item_detail_id = $Item_detail->id;
                    $item_feature_detail->item_feature_id = $i;
                    $item_feature_detail->status = '0';
                    $item_feature_detail->save();
                    continue;
                }
                $item_feature_detail->Item_detail_id = $Item_detail->id;
                $item_feature_detail->item_feature_id = $i;
                $item_feature_detail->status = $request->$i;
                $item_feature_detail->save();
            }


            for ($i=1; $i <= 30; $i++) { 
                $item_technical_detail = new ItemTechnicalDetail;
                $technical_id = 'technical_' . $i;
                $item_technical_detail->Item_detail_id = $Item_detail->id;
                $item_technical_detail->Item_technical_id = $i;
                $item_technical_detail->value = $request->$technical_id;
                $item_technical_detail->save();

            }

            $i=1;
            foreach ($request->file('photo') as $file) {
                $ext = $file->guessClientExtension();
                $imageName = time() . str_random(3) . '.' . $ext;

                $photo = new Photo;

                $photo->fileName = $imageName;
                $photo->Item_detail_id = $Item_detail->id;
                $photo->priority = $i;
                $photo->save();
                $file->storeAs('item/img/' . $Item_detail->id . '/', $imageName);
                //$filePath = 'item/img/' . $Item_detail->id . '/' . $photo->fileNgame;
                //Storage::put($filePath, file_get_contents($file[$i]), 'public');
                $i += 1;
            }



            DB::commit();
        } catch (Exception $e) {
                DB::rollBack();
            }

        return redirect()->back()->with('success', "პროექტი წარმატებით დაემატა ბაზაში!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brands = ItemBrend::all();
        $models = ItemModel::all();
        $body_styles = ItemBodyStyle::all();
        $features = ItemFeature::all();
        $technicals = ItemTechnical::all();
        $detail = ItemDetail::find($id);
        $feature_detail = ItemFeatureDetail::where('item_detail_id', $id)->get();
        $technical_detail = ItemTechnicalDetail::where('item_detail_id', $id)->get();
        return view('admin.inventory.create_edit', 
                    compact('brands', 'models', 'body_styles', 'features', 'technicals', 'detail',
                            'feature_detail', 'technical_detail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
