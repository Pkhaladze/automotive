<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ItemDetail;
use App\Models\ItemBrend;
use App\Models\ItemFeatureDetail;
use App\Models\ItemFeature;
use App\Models\ItemModel;
use App\Models\ItemBodyStyle;
use App\Models\ItemTechnicalDetail;
use App\Models\ItemTechnicalCatalog;
use App\Models\ItemTechnical;
use Carbon\Carbon;

class SiteController extends Controller
{



    public function test()
    {
        $active_link = "details";
        $detail = ItemDetail::find(1);
        $technicals = ItemTechnicalDetail::where('item_detail_id', 1)->get();
        $technical_catalogs = ItemTechnicalCatalog::all();
        $items = ItemDetail::all();
        $features = ItemFeature::all();
        $time = $detail->year;
        $date = new Carbon( $time ); 
        return view('test', compact('active_link', 'date', 'detail', 'technicals', 'features', '
                                    technical_catalogs', 'items'));
    }


    public function index()
    {
        $active_link = "home";
        $date_now = date('Y');
        $date_before = $date_now - 20;
        return view('site.index', compact('date_now', 'date_before', 'active_link'));
    }

    public function inventory()
    {
        $active_link = "inventory";
        $technicals = ItemTechnicalDetail::all();
        $items = ItemDetail::all();
        $brends = ItemBrend::all();
        $body_styles = ItemBodyStyle::all();
        $date_now = date('Y');
        $date_before = $date_now - 20;
        return view('site.inventory', compact('date_now', 'date_before', 'active_link', 'brends',
                                            'body_styles', 'items', 'technicals'));
    }

    public function store_message(Request $request)
    {
        
        $k = $request->all();
        dd($k);
        dd( $request);
    } 

    public function service()
    {
        $active_link = "service";
        return view('site.service', compact('active_link'));
    }
    
    public function our_team()
    {
        $active_link = "our_team";
        return view('site.our_team', compact('active_link'));
    }

    public function portfolio()
    {
        $active_link = "portfolio";
        return view('site.portfolio', compact('active_link'));
    }

    public function trade()
    {
        $active_link = "trade";
        return view('site.sub_pages.trade', compact('active_link'));
    }

    public function about()
    {
        $active_link = "about";
        return view('site.about', compact('active_link'));
    }

    public function contact()
    {
        $active_link = "contact";
        return view('site.contact', compact('active_link'));
    }


    public function item_details($id)
    {
        $active_link = "inventory";
        $detail = ItemDetail::find($id);
        $technical_details = ItemTechnicalDetail::where('item_detail_id', $id)->get();
        $technical_catalogs = ItemTechnicalCatalog::all();
        $technicals = ItemTechnical::all();
        $features = ItemFeatureDetail::where('item_detail_id', $id)->get();
        $time = $detail->year;
        $date = new Carbon( $time ); 
        /*dd($id);*/
        return view('site.sub_pages.details', compact('active_link', 'date', 'detail', 'technicals', 'features', 'technical_catalogs', 'technical_details'));
    }

}
