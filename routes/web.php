<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/test', 'SiteController@test');

Route::post('/trade_in', 'SiteController@store_message');
Route::get('/', 'SiteController@index');
Route::get('/inventory', 'SiteController@inventory');
Route::get('/trade', 'SiteController@trade');
Route::get('/service', 'SiteController@service');
Route::get('/our_team', 'SiteController@our_team');
Route::get('/portfolio', 'SiteController@portfolio');
Route::get('/about', 'SiteController@about');
Route::get('/contact', 'SiteController@contact');
Route::get('/details/{id}', 'SiteController@item_details');



Auth::routes();

Route::get('/home', 'HomeController@index');


Route::group(['prefix' => 'admin'], function () {
    Route::get('/', 'AdminPageController@index');
    Route::resource('/inventory', 'AdminItemController');
});