<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('price')->unsigned();
            $table->enum('condition', ['new', 'used'])->default('used');
            $table->string('location')->nullable();
            $table->date('year');
            $table->text('comment')->nullable();
            $table->mediumText('overview')->nullable();
            $table->integer('item_brend_id')->unsigned();
            $table->foreign('item_brend_id')->references('id')
                                        ->on('item_brends')
                                        ->onDelete('cascade');
            $table->integer('item_model_id')->unsigned();
            $table->foreign('item_model_id')->references('id')
                                        ->on('item_models')
                                        ->onDelete('cascade');
            $table->integer('item_body_style_id')->unsigned();
            $table->foreign('item_body_style_id')->references('id')
                                        ->on('item_body_styles')
                                        ->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_details');
    }
}
