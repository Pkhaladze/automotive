<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemTechnicalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_technicals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->integer('item_technical_catalog_id')->unsigned();
            $table->foreign('item_technical_catalog_id')->references('id')
                                        ->on('item_technical_catalogs')
                                        ->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_technicals');
    }
}
