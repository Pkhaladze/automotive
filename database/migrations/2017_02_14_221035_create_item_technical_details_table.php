<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemTechnicalDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_technical_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_detail_id')->unsigned();
            $table->foreign('item_detail_id')->references('id')
                                        ->on('item_details')
                                        ->onDelete('cascade');
/*            $table->integer('item_technical_catalog_id')->unsigned();
            $table->foreign('item_technical_catalog_id')->references('id')
                                        ->on('item_technical_catalogs')
                                        ->onDelete('cascade');*/
            $table->integer('item_technical_id')->unsigned();
            $table->foreign('item_technical_id')->references('id')
                                        ->on('item_technicals')
                                        ->onDelete('cascade');
            $table->string('value')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_technical_details');
    }
}
