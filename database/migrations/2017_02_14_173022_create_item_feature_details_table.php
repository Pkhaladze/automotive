<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemFeatureDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_feature_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_detail_id')->unsigned();
            $table->foreign('item_detail_id')->references('id')
                                        ->on('item_details')
                                        ->onDelete('cascade');
            $table->integer('item_feature_id')->unsigned();
            $table->foreign('item_feature_id')->references('id')
                                        ->on('item_features')
                                        ->onDelete('cascade');
            $table->tinyInteger('status')->unsigned();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_feature_details');
    }
}
