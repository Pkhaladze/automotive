<?php

use Illuminate\Database\Seeder;

class ItemBrendsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('item_brends')->insert([
            [   
                'name' => 'Mazda',
            ],
            [   
                'name' => 'Mercedes-Benz',
            ],
            [   
                'name' => 'Mitsubishi',
            ],

            [   
                'name' => 'Volkswagen',
            ],
            [   
                'name' => 'Volvo',
            ],
        ]);
    }
}
