<?php

use Illuminate\Database\Seeder;

class ItemBodyStylesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('item_body_styles')->insert([
            [
                'name' => 'Cargo',
            ],
            [
                'name' => 'Compact',
            ],
            [
                'name' => 'Convertible',
            ],
            [
                'name' => 'Coupe',
            ],
            [
                'name' => 'Hatchback',
            ],
            [
                'name' => 'Minivan',
            ],
            [
                'name' => 'Sedan',
            ],
            [
                'name' => 'SUV',
            ],
            [
                'name' => 'Truck',
            ],
            [
                'name' => 'Van',
            ],
            [
                'name' => 'Wagon',
            ],
        ]);
    }
}