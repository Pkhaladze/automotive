<?php

use Illuminate\Database\Seeder;

class ItemTechnicalsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('item_technicals')->insert([
            [
                'name' => 'Drivetrain',
                'item_technical_catalog_id' => '1',
            ],
            [
                'name' => 'Exterior Color',
                'item_technical_catalog_id' => '1',
            ],
            [
                'name' => 'Interior Color',
                'item_technical_catalog_id' => '1',
            ],
            [
                'name' => 'Mileage',
                'item_technical_catalog_id' => '1',
            ],
            [
                'name' => 'Doors',
                'item_technical_catalog_id' => '1',
            ],
            [
                'name' => 'Passengers',
                'item_technical_catalog_id' => '1',
            ],
            [
                'name' => 'Vin #:',
                'item_technical_catalog_id' => '1',
            ],
            [
                'name' => 'Fuel Type:',
                'item_technical_catalog_id' => '1',
            ],
            [
                'name' => 'Owners:',
                'item_technical_catalog_id' => '1',
            ],
            [
                'name' => 'Warranty:',
                'item_technical_catalog_id' => '1',
            ],
            [
                'name' => 'Layout / number of cylinders',
                'item_technical_catalog_id' => '2',
            ],
            [
                'name' => 'Displacement',
                'item_technical_catalog_id' => '2',
            ],
            [
                'name' => 'Engine Layout',
                'item_technical_catalog_id' => '2',
            ],
            [
                'name' => 'Horespower',
                'item_technical_catalog_id' => '2',
            ],
            [
                'name' => '@ rpm',
                'item_technical_catalog_id' => '2',
            ],
            [
                'name' => 'Torque',
                'item_technical_catalog_id' => '2',
            ],
            [
                'name' => 'Compression ratio',
                'item_technical_catalog_id' => '2',
            ],
            [
                'name' => 'Top Track Speed',
                'item_technical_catalog_id' => '3',
            ],
            [
                'name' => '0 - 60 mph',
                'item_technical_catalog_id' => '3',
            ],
            [
                'name' => 'Manual Gearbox',
                'item_technical_catalog_id' => '4',
            ],
            [
                'name' => 'City (estimate)',
                'item_technical_catalog_id' => '5',
            ],
            [
                'name' => 'Highway (estimate)',
                'item_technical_catalog_id' => '5',
            ],
            [
                'name' => 'Length',
                'item_technical_catalog_id' => '6',
            ],
            [
                'name' => 'Width',
                'item_technical_catalog_id' => '6',
            ],
            [
                'name' => 'Height',
                'item_technical_catalog_id' => '6',
            ],
            [
                'name' => 'Wheelbase',
                'item_technical_catalog_id' => '6',
            ],
            [
                'name' => 'Maximum payload',
                'item_technical_catalog_id' => '6',
            ],
            [
                'name' => 'Curb weight',
                'item_technical_catalog_id' => '6',
            ],
            [
                'name' => 'Luggage compartment volume',
                'item_technical_catalog_id' => '7',
            ],
            [
                'name' => 'Fuel Tank Capacity',
                'item_technical_catalog_id' => '7',
            ],
        ]);
    }
}
