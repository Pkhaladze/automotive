<?php

use Illuminate\Database\Seeder;

class ItemModelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('item_models')->insert([
            [
                'name' => 'Familia',
                'item_brend_id' => '1',
            ],
            [
                'name' => 'MPV',
                'item_brend_id' => '1',
            ],
            [
                'name' => 'CLK GTR',
                'item_brend_id' => '2',
            ],
            [
                'name' => 'SLR McLaren',
                'item_brend_id' => '2',
            ],
            [
                'name' => 'Galant',
                'item_brend_id' => '3',
            ],
            [
                'name' => 'Pajero',
                'item_brend_id' => '3',
            ],
            [
                'name' => 'Jetta',
                'item_brend_id' => '4',
            ],
            [
                'name' => 'Passat',
                'item_brend_id' => '4',
            ],
            [
                'name' => '200 series',
                'item_brend_id' => '5',
            ],
            [
                'name' => '300 series',
                'item_brend_id' => '5',
            ],
            
        ]);
    }
}
