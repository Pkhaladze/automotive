<?php

use Illuminate\Database\Seeder;

class OptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('trade_options')->insert([
            [
                'name' => 'body',
            ],
            [
                'name' => 'drivetrain',
            ],
            [
                'name' => 'exterior',
            ],
            [
                'name' => 'interior',
            ],
            [
                'name' => 'electronics',
            ],
            [
                'name' => 'safety features',
            ]
        ]   
        );
    }
}
 
