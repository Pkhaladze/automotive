<?php

use Illuminate\Database\Seeder;

class ItemTechnicalCatalogsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('item_technical_catalogs')->insert([
            [
                'name' => 'General',
            ],
            [
                'name' => 'Engine',
            ],
            [
                'name' => 'Performance',
            ],
            [
                'name' => 'Transmission',
            ],
            [
                'name' => 'Fuel consumption',
            ],
            [
                'name' => 'Body',
            ],
            [
                'name' => 'Capacities',
            ],
        ]);
    }
}
