<?php

use Illuminate\Database\Seeder;

class ItemTechnicalDetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('item_technical_details')->insert([
            [
                'item_detail_id' => '1',
                /*'item_technical_catalog_id' => '1',*/
                'item_technical_id' => '1',
                'value' => 'Rear Wheel Drive',
            ],
            [
                'item_detail_id' => '1',
                /*'item_technical_catalog_id' => '1',*/
                'item_technical_id' => '2',
                'value' => 'Lime Gold Metallic',
            ],
            [
                'item_detail_id' => '1',
                /*'item_technical_catalog_id' => '1',*/
                'item_technical_id' => '3',
                'value' => 'Agate Grey',
            ],
            [
                'item_detail_id' => '1',
                /*'item_technical_catalog_id' => '1',*/
                'item_technical_id' => '4',
                'value' => '12',
            ],
            [
                'item_detail_id' => '1',
                /*'item_technical_catalog_id' => '1',*/
                'item_technical_id' => '5',
                'value' => '2',
            ],
            [
                'item_detail_id' => '1',
                /*'item_technical_catalog_id' => '1',*/
                'item_technical_id' => '6',
                'value' => '2',
            ],
            [
                'item_detail_id' => '1',
                /*'item_technical_catalog_id' => '1',*/
                'item_technical_id' => '7',
                'value' => 'WP0AB2E81EK190171',
            ],
            [
                'item_detail_id' => '1',
                /*'item_technical_catalog_id' => '1',*/
                'item_technical_id' => '8',
                'value' => 'Gasoline',
            ],
            [
                'item_detail_id' => '1',
                /*'item_technical_catalog_id' => '1',*/
                'item_technical_id' => '9',
                'value' => 'Merabi',
            ],
            [
                'item_detail_id' => '1',
                /*'item_technical_catalog_id' => '1',*/
                'item_technical_id' => '10',
                'value' => '3 Years Limited',
            ],
            [
                'item_detail_id' => '1',
                /*'item_technical_catalog_id' => '2',*/
                'item_technical_id' => '11',
                'value' => '6',
            ],
            [
                'item_detail_id' => '1',
                /*'item_technical_catalog_id' => '2',*/
                'item_technical_id' => '12',
                'value' => '3.4 l',
            ],
            [
                'item_detail_id' => '1',
                /*'item_technical_catalog_id' => '2',*/
                'item_technical_id' => '13',
                'value' => 'Mid-engine',
            ],
            [
                'item_detail_id' => '1',
                /*'item_technical_catalog_id' => '2',*/
                'item_technical_id' => '14',
                'value' => '315 hp',
            ],
            [
                'item_detail_id' => '1',
                /*'item_technical_catalog_id' => '2',*/
                'item_technical_id' => '15',
                'value' => '6,700 rpm',
            ],
            [
                'item_detail_id' => '1',
                /*'item_technical_catalog_id' => '2',*/
                'item_technical_id' => '16',
                'value' => '266 lb.-ft.',
            ],
            [
                'item_detail_id' => '1',
                /*'item_technical_catalog_id' => '2',*/
                'item_technical_id' => '17',
                'value' => '12.5 : 1',
            ],
            [
                'item_detail_id' => '1',
                /*'item_technical_catalog_id' => '3',*/
                'item_technical_id' => '18',
                'value' => '173 mph',
            ],
            [
                'item_detail_id' => '1',
                /*'item_technical_catalog_id' => '3',*/
                'item_technical_id' => '19',
                'value' => '4.8 s',
            ],
            [
                'item_detail_id' => '1',
                /*'item_technical_catalog_id' => '4',*/
                'item_technical_id' => '20',
                'value' => '6-speed Manual',
            ],
            [
                'item_detail_id' => '1',
                /*'item_technical_catalog_id' => '5',*/
                'item_technical_id' => '21',
                'value' => '20',
            ],
            [
                'item_detail_id' => '1',
                /*'item_technical_catalog_id' => '5',*/
                'item_technical_id' => '22',
                'value' => '28',
            ],
            [
                'item_detail_id' => '1',
                /*'item_technical_catalog_id' => '6',*/
                'item_technical_id' => '23',
                'value' => '172.2 in.',
            ],
            [
                'item_detail_id' => '1',
                /*'item_technical_catalog_id' => '6',*/
                'item_technical_id' => '24',
                'value' => '70.9 in.',
            ],
            [
                'item_detail_id' => '1',
                /*'item_technical_catalog_id' => '6',*/
                'item_technical_id' => '26',
                'value' => '50.4 in.',
            ],
            [
                'item_detail_id' => '1',
                /*'item_technical_catalog_id' => '6',*/
                'item_technical_id' => '25',
                'value' => '97.4 in.',
            ],
            [
                'item_detail_id' => '1',
                /*'item_technical_catalog_id' => '6',*/
                'item_technical_id' => '27',
                'value' => '739 lbs',
            ],
            [
                'item_detail_id' => '1',
                /*'item_technical_catalog_id' => '6',*/
                'item_technical_id' => '28',
                'value' => '2910 lbs',
            ],
            [
                'item_detail_id' => '1',
                /*'item_technical_catalog_id' => '7',*/
                'item_technical_id' => '29',
                'value' => '5.3 cu. ft. (front) / 4.6 cu. ft. (rear)',
            ],
            [
                'item_detail_id' => '1',
                /*'item_technical_catalog_id' => '7',*/
                'item_technical_id' => '30',
                'value' => '16.9 L',
            ],
            [
                'item_detail_id' => '2',
                /*'item_technical_catalog_id' => '1',*/
                'item_technical_id' => '1',
                'value' => 'front Wheel Drive',
            ],
            [
                'item_detail_id' => '2',
                /*'item_technical_catalog_id' => '1',*/
                'item_technical_id' => '2',
                'value' => 'Lime sylver Metallic',
            ],
            [
                'item_detail_id' => '2',
                /*'item_technical_catalog_id' => '1',*/
                'item_technical_id' => '3',
                'value' => 'Agate Grey',
            ],
            [
                'item_detail_id' => '2',
                /*'item_technical_catalog_id' => '1',*/
                'item_technical_id' => '4',
                'value' => '1200',
            ],
            [
                'item_detail_id' => '2',
                /*'item_technical_catalog_id' => '1',*/
                'item_technical_id' => '5',
                'value' => '4',
            ],
            [
                'item_detail_id' => '2',
                /*'item_technical_catalog_id' => '1',*/
                'item_technical_id' => '6',
                'value' => '5',
            ],
            [
                'item_detail_id' => '2',
                /*'item_technical_catalog_id' => '1',*/
                'item_technical_id' => '7',
                'value' => 'WP0AB2E81EK190122',
            ],
            [
                'item_detail_id' => '2',
                /*'item_technical_catalog_id' => '1',*/
                'item_technical_id' => '8',
                'value' => 'Gas',
            ],
            [
                'item_detail_id' => '2',
                /*'item_technical_catalog_id' => '1',*/
                'item_technical_id' => '9',
                'value' => 'N/A',
            ],
            [
                'item_detail_id' => '2',
                /*'item_technical_catalog_id' => '1',*/
                'item_technical_id' => '10',
                'value' => '2 Years Limited',
            ],
            [
                'item_detail_id' => '2',
                /*'item_technical_catalog_id' => '2',*/
                'item_technical_id' => '11',
                'value' => '8',
            ],
            [
                'item_detail_id' => '2',
                /*'item_technical_catalog_id' => '2',*/
                'item_technical_id' => '12',
                'value' => '3.4 l',
            ],
            [
                'item_detail_id' => '2',
                /*'item_technical_catalog_id' => '2',*/
                'item_technical_id' => '13',
                'value' => 'Mid-engine',
            ],
            [
                'item_detail_id' => '2',
                /*'item_technical_catalog_id' => '2',*/
                'item_technical_id' => '14',
                'value' => '400 hp',
            ],
            [
                'item_detail_id' => '2',
                /*'item_technical_catalog_id' => '2',*/
                'item_technical_id' => '15',
                'value' => '9,700 rpm',
            ],
            [
                'item_detail_id' => '2',
                /*'item_technical_catalog_id' => '2',*/
                'item_technical_id' => '16',
                'value' => '300 lb.-ft.',
            ],
            [
                'item_detail_id' => '2',
                /*'item_technical_catalog_id' => '2',*/
                'item_technical_id' => '17',
                'value' => '12.5 : 1',
            ],
            [
                'item_detail_id' => '2',
                /*'item_technical_catalog_id' => '3',*/
                'item_technical_id' => '18',
                'value' => '173 mph',
            ],
            [
                'item_detail_id' => '2',
                /*'item_technical_catalog_id' => '3',*/
                'item_technical_id' => '19',
                'value' => '4.8 s',
            ],
            [
                'item_detail_id' => '2',
                /*'item_technical_catalog_id' => '4',*/
                'item_technical_id' => '20',
                'value' => '6-speed Manual',
            ],
            [
                'item_detail_id' => '2',
                /*'item_technical_catalog_id' => '5',*/
                'item_technical_id' => '21',
                'value' => '50',
            ],
            [
                'item_detail_id' => '2',
                /*'item_technical_catalog_id' => '5',*/
                'item_technical_id' => '22',
                'value' => '80',
            ],
            [
                'item_detail_id' => '2',
                /*'item_technical_catalog_id' => '6',*/
                'item_technical_id' => '23',
                'value' => '170.2 in.',
            ],
            [
                'item_detail_id' => '2',
                /*'item_technical_catalog_id' => '6',*/
                'item_technical_id' => '24',
                'value' => '70.9 in.',
            ],
            [
                'item_detail_id' => '2',
                /*'item_technical_catalog_id' => '6',*/
                'item_technical_id' => '26',
                'value' => '50.4 in.',
            ],
            [
                'item_detail_id' => '2',
                /*'item_technical_catalog_id' => '6',*/
                'item_technical_id' => '25',
                'value' => '97.4 in.',
            ],
            [
                'item_detail_id' => '2',
                /*'item_technical_catalog_id' => '6',*/
                'item_technical_id' => '27',
                'value' => '739 lbs',
            ],
            [
                'item_detail_id' => '2',
                /*'item_technical_catalog_id' => '6',*/
                'item_technical_id' => '28',
                'value' => '2910 lbs',
            ],
            [
                'item_detail_id' => '2',
                /*'item_technical_catalog_id' => '7',*/
                'item_technical_id' => '29',
                'value' => '6.3 cu. ft. (front) / 5.6 cu. ft. (rear)',
            ],
            [
                'item_detail_id' => '2',
                /*'item_technical_catalog_id' => '7',*/
                'item_technical_id' => '30',
                'value' => '120 L',
            ],
        ]);
    }
}
