<?php

use Illuminate\Database\Seeder;

class ItemFeaturesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('item_features')->insert([
            [
                'name' => 'Adaptive Cruise Control',
            ],
            [
                'name' => 'Airbags',
            ],
            [
                'name' => 'Air Conditioning',
            ],
            [
                'name' => 'Alarm System',
            ],
            [
                'name' => 'Anti-theft Protection',
            ],
            [
                'name' => 'Audio Interface',
            ],
            [
                'name' => 'Automatic Climate Control',
            ],
            [
                'name' => 'Automatic Headlights',
            ],
            [
                'name' => 'Auto Start/Stop',
            ],
            [
                'name' => 'Bi-Xenon Headlights',
            ],
            [
                'name' => 'Bluetooth® Handset',
            ],
            [
                'name' => 'BOSE® Surround Sound',
            ],
            [
                'name' => 'Burmester® Surround Sound',
            ],
            [
                'name' => 'CD/DVD Autochanger',
            ],
            [
                'name' => 'CDR Audio',
            ],
            [
                'name' => 'Cruise Control',
            ],
            [
                'name' => 'Direct Fuel Injection',
            ],
            [
                'name' => 'Electric Parking Brake',
            ],
            [
                'name' => 'Floor Mats',
            ],
            [
                'name' => 'Garage Door Opener',
            ],
            [
                'name' => 'Leather Package',
            ],
            [
                'name' => 'Locking Rear Differential',
            ],
            [
                'name' => 'Luggage Compartments',
            ],
            [
                'name' => 'Manual Transmission',
            ],
            [
                'name' => 'Navigation Module',
            ],
            [
                'name' => 'Online Services',
            ],
            [
                'name' => 'ParkAssist',
            ],
            [
                'name' => 'Porsche Communication',
            ],
            [
                'name' => 'Power Steering',
            ],
            [
                'name' => 'Reversing Camera',
            ],
            [
                'name' => 'Roll-over Protection',
            ],
            [
                'name' => 'Seat Heating',
            ],
            [
                'name' => 'Seat Ventilation',
            ],
            [
                'name' => 'Sound Package Plus',
            ],
            [
                'name' => 'Sport Chrono Package',
            ],
            [
                'name' => 'Steering Wheel Heating',
            ],
            [
                'name' => 'Tire Pressure Monitoring',
            ],
            [
                'name' => 'Universal Audio Interface',
            ],
            [
                'name' => 'Voice Control System',
            ],
            [
                'name' => 'Wind Deflector',
            ],
        ]);
    }
}










