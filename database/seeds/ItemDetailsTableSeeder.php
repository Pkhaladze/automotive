<?php

use Illuminate\Database\Seeder;

class ItemDetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('item_details')->insert([
            [   
                'price' => '71995',
                'condition' => 'used',
                'location' => '39.917960, 32.872271',
                'year' => '2013-02-07',
                'comment' => 'Vestibulum sit amet ligula eget nibh cursus bibendum et id eros. 
                            Nam congue, dui quis consectetur blandit, neque neque mattis diam, vitae egestas urna lectus eu turpis. In vitae commodo sem. Etiam vehicula sed ligula malesuada cursus.',
                'overview' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec 
                                luctus tincidunt aliquam. Aliquam gravida massa at sem vulputate interdum
                                <p>Vestibulum vel mauris et odio lobortis laoreet eget eu magna. Proin mauris erat, luctus at nulla ut, lobortis mattis magna. Morbi a arcu lacus. Maecenas tristique velit vitae nisi consectetur, in mattis diam sodales. Mauris sagittis sem mattis justo bibendum, a eleifend dolor facilisis. Mauris nec pharetra tortor, ac aliquam felis. Nunc pretium erat sed quam consectetur fringilla.</p>',
                'item_brend_id' => '5',
                'item_model_id' => '9',
                'item_body_style_id' => '2',
            ],
            [   
                'price' => '80000',
                'condition' => 'new',
                'location' => '39.917960, 32.872271',
                'year' => '2017-02-07',
                'comment' => '2017 Vestibulum sit amet ligula eget nibh cursus bibendum et id eros. 
                            Nam congue, dui quis consectetur blandit, neque neque mattis diam, vitae egestas urna lectus eu turpis. In vitae commodo sem. Etiam vehicula sed ligula malesuada cursus.',
                'overview' => '2016 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec 
                                luctus tincidunt aliquam. Aliquam gravida massa at sem vulputate interdum
                                <p>2 Vestibulum vel mauris et odio lobortis laoreet eget eu magna. Proin mauris erat, luctus at nulla ut, lobortis mattis magna. Morbi a arcu lacus. Maecenas tristique velit vitae nisi consectetur, in mattis diam sodales. Mauris sagittis sem mattis justo bibendum, a eleifend dolor facilisis. Mauris nec pharetra tortor, ac aliquam felis. Nunc pretium erat sed quam consectetur fringilla.</p>',
                'item_brend_id' => '2',
                'item_model_id' => '6',
                'item_body_style_id' => '4',
            ],
        ]);
    }
}
