<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(ItemFeaturesTableSeeder::class);
        $this->call(ItemTechnicalCatalogsTableSeeder::class);
        $this->call(ItemTechnicalsTableSeeder::class);
        $this->call(ItemBrendsTableSeeder::class);
        $this->call(ItemModelsTableSeeder::class);
        $this->call(ItemFeatureDetailsTableSeeder::class);
        $this->call(ItemTechnicalDetailsTableSeeder::class);
        $this->call(ItemDetailsTableSeeder::class);
        $this->call(ItemBodyStylesTableSeeder::class);
    }
}
