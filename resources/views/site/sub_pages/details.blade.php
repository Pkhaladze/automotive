@extends('layouts.site_master')
@section('content')
<section id="secondary-banner" class="dynamic-image-8"><!--for other images just change the class name of this section block like, class="dynamic-image-2" and add css for the changed class-->
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                <h2>Inventory Listing</h2>
                <h4>Powerful Inventory Marketing, Fully Integrated</h4>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 ">
                <ul class="breadcrumb">
                    <li><a href="{{ asset('/') }}">Home</a></li>
                    <li><a href="{{ asset('/inventory') }}">Inventory</a></li>
                    <li>Details</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--secondary-banner ends-->
<div class="message-shadow"></div>
<div class="clearfix"></div>
<section class="content">
    <div class="container">
        <div class="inner-page inventory-listing">
            <div class="inventory-heading margin-bottom-10 clearfix">
                <div class="row">
                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                        <h2>{{$date->year}} {{$detail->item_brend->name}} {{$detail->item_model->name}} Type {{$detail->item_body_style->name}}</h2>
                        <span class="margin-top-10">Our template platform will maximize the exposure of your inventory online</span> </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 text-right">
                        <h2>$ {{number_format($detail->price)}}</h2>
                        <em>Plus Taxes & Licensing</em> </div>
                </div>
            </div>
            <div class="content-nav margin-bottom-30">
                <div class="row">
                    <ul>
                        <li class="prev1 gradient_button"><a href="#">Prev Vehicle</a></li>
                        <li class="request gradient_button"><a href="{{ asset('forms/request.php?recaptcha') }}" class="fancybox_div">Request More Info</a></li>
                        <li class="schedule gradient_button"><a href="{{ asset('forms/schedule.php?recaptcha') }}" class="fancybox_div">Schedule Test Drive</a></li>
                        <li class="offer gradient_button"><a href="{{ asset('forms/offer.php?recaptcha') }}" class="fancybox_div">Make an Offer</a></li>
                        {{-- <li class="trade gradient_button"><a href="forms/trade.php?recaptcha" class="fancybox_div">Trade-In Appraisal</a></li> --}}
                        <li class="pdf gradient_button"><a href="#" class="">PDF Brochure</a></li>
                        <li class="print gradient_button"><a class="print_page">Print This Vehicle</a></li>
                        {{-- <li class="email gradient_button"><a href="forms/friend.php?recaptcha" class="fancybox_div">Email to a Friend</a></li> --}}
                        <li class="next1 gradient_button"><a href="#">Next Vehicle</a></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 left-content padding-left-none"> 
                    <!--OPEN OF SLIDER-->
                    <div class="listing-slider">
                        <section class="slider home-banner">
                            <div class="flexslider" id="home-slider-canvas">
                                <ul class="slides">
                                    <li data-thumb="{{ asset('images/boxster1_slide.jpg') }}"> <img src="{{ asset('images/boxster1_slide.jpg') }}" alt="" data-full-image="{{ asset('images/boxster1.jpg') }}" /> </li>
                                    <!-- full -->
                                    <li data-thumb="{{ asset('images/boxster2_slide.jpg') }}"> <img src="{{ asset('images/boxster2_slide.jpg') }}" alt="" data-full-image="{{ asset('images/boxster2.jpg') }}" /> </</li>
                                </ul>
                            </div>
                        </section>
                        <section class="home-slider-thumbs">
                            <div class="flexslider" id="home-slider-thumbs">
                                <ul class="slides">
                                    <li data-thumb="{{ asset('images/thumbnail1.jpg') }}"> <a href="#"><img src="{{ asset('images/thumbnail1.jpg') }}" alt="" /></a> </li>
                                    <!-- full -->
                                    <li data-thumb="{{ asset('images/thumbnail6.jpg') }}"> <a href="#"><img src="{{ asset('images/thumbnail6.jpg') }}" alt="" /></a> </li>
                                    
                                </ul>
                            </div>
                        </section>
                    </div>
                    <!--CLOSE OF SLIDER--> 
                    <!--Slider End-->
                    <div class="clearfix"></div>
                    <div class="bs-example bs-example-tabs example-tabs margin-top-50">
                        <ul id="myTab" class="nav nav-tabs">
                            <li class="active"><a href="#vehicle" data-toggle="tab">Vehicle Overview</a></li>
                            <li><a href="#features" data-toggle="tab">Features &amp; Options</a></li>
                            <li><a href="#technical" data-toggle="tab">Technical Specifications</a></li>
                            <li><a href="#location" data-toggle="tab">Vehicle Location</a></li>
                            <li><a href="#comments" data-toggle="tab">Other Comments</a></li>
                        </ul>
                        <div id="myTabContent" class="tab-content margin-top-15 margin-bottom-20">
                            <div class="tab-pane fade in active" id="vehicle">
                                <p>{!! $detail->overview !!}</p>
                            </div>
                            <div class="tab-pane fade" id="features">
                                <ul class="fa-ul">
                                @foreach ($features as $feature)
                                    <li>
                                    @if ($feature->status == 1)
                                        <i class="fa-li fa fa-check"></i> 
                                    @endif
                                    {{$feature->item_feature->name}}</li>
                                @endforeach
                                    
                                </ul>
                            </div>
                            <div class="tab-pane fade" id="technical">
                                @foreach ($technical_catalogs as $technical_catalog)
                                    <table class="technical">
                                        <thead>
                                        <tr>
                                            <th>{{$technical_catalog->name}}</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php
                                            $techs = $technicals->where("item_technical_catalog_id", $technical_catalog->id);

                                        @endphp
                                        @foreach ($techs as $tech)
                                            @php
                                                $tech_detail = $technical_details->where('item_technical_id', $tech->id);
                                            @endphp
                                    {{--         {{dd($tech_detail)}}
                                            {{dd($techs)}} --}}
                                            <tr>
                                                <td>{{$tech->name}}</td>
                                                <td></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                @endforeach
                            </div>
                            <div class="tab-pane fade" id="location">
                                <div id='google-map-listing' class="contact" data-longitude='-79.38' data-latitude='43.65' data-zoom='11' style="height: 350px;" data-parallax="false"></div>
                            </div>
                            <div class="tab-pane fade" id="comments">
                                <p>{!! $detail->comment !!}</p>
                                {{-- <p><img src="images/engine.png" alt="engine" /></p> --}}
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 right-content padding-right-none">
                    <div class="side-content">
                        <div class="car-info margin-bottom-50">
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                            @foreach ($technical_details as $technical_detail)
                                                @php
                                                    $tech[$technical_detail->item_technical->name] = $technical_detail->value;
                                                @endphp
                                            @endforeach
                                        <tr>
                                            <td>Body Style:</td>
                                            <td>{{$detail->item_body_style->name}}</td>
                                        </tr>
                                        <tr>
                                            <td>ENGINE:</td>
                                            <td>{{$tech['Displacement']}} {{$tech['Engine Layout']}} V{{$tech['Layout / number of cylinders']}}</td>
                                        </tr>
                                        <tr>
                                            <td>TRANSMISSION:</td>
                                            <td>{{$tech['Manual Gearbox']}}</td>
                                        </tr>
                                        <tr>
                                            <td>DRIVETRAIN:</td>
                                            <td>{{$tech['Drivetrain']}}</td>
                                        </tr>
                                        <tr>
                                            <td>EXTERIOR:</td>
                                            <td>{{$tech['Exterior Color']}}</td>
                                        </tr>
                                        <tr>
                                            <td>INTERIOR:</td>
                                            <td>{{$tech['Interior Color']}}</td>
                                        </tr>
                                        <tr>
                                            <td>MILES:</td>
                                            <td>{{$tech['Mileage']}}</td>
                                        </tr>
                                        <tr>
                                            <td>DOORS:</td>
                                           <td>{{$tech['Doors']}}</td>
                                        </tr>
                                        <tr>
                                            <td>PASSENGERS:</td>
                                            <td>{{$tech['Passengers']}}</td>
                                        </tr>
                                        <tr>
                                            <td>STOCK #:</td>
                                            <td>{{$detail->id}}</td>
                                        </tr>
                                        <tr>
                                            <td>VIN #:</td>
                                            <td>{{$tech['Vin #:']}}</td>
                                        </tr>
                                        <tr>
                                            <td>FUEL MILEAGE:</td>
                                            <td>{{$tech['City (estimate)']}} City / {{$tech['Highway (estimate)']}}  Hwy</td>
                                        </tr>
                                        <tr>
                                            <td>FUEL TYPE:</td>
                                            <td>{{$tech['Fuel Type:']}}</td>
                                        </tr>
                                        <tr>
                                            <td>CONDITION:</td>
                                            <td>{{$detail->condition}}</td>
                                        </tr>
                                        <tr>
                                            <td>WARRANTY:</td>
                                            <td>{{$tech['Warranty:']}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="efficiency-rating text-center padding-vertical-15 margin-bottom-40">
                            <h3>Fuel Efficiency Rating</h3>
                            <ul>
                                <li class="city_mpg"><small>City MPG:</small> <strong>{{$tech['City (estimate)']}}</strong></li>
                                <li class="fuel"><img src="{{ asset('images/fuel-icon.png') }}" alt="" class="aligncenter"></li>
                                <li class="hwy_mpg"><small>Hwy MPG:</small> <strong>{{$tech['Highway (estimate)']}} </strong></li>
                            </ul>
                            <p>Actual rating will vary with options, driving conditions,
                                driving habits and vehicle condition.</p>
                        </div>
                        <ul class="social-likes pull-right" data-url="https://themesuite.com" data-title="Blog Post">
                            <li class="facebook" title="Share link on Facebook"></li>
                            <li class="plusone" title="Share link on Google+" data-counter="includes/googleplusonecount.php?url=https://google.com&amp;callback=?"></li>
                            <li class="pinterest" title="Share image on Pinterest" data-media=""></li>
                            <li class="twitter" title="Share link on Twitter"></li>
                        </ul>
                        <div class="clearfix"></div>
                        <div class="financing_calculator margin-top-40">
                            <h3>FINANCING CALCULATOR</h3>
                            <div class="table-responsive">
                                <table class="table no-border no-margin">
                                    <tbody>
                                        <tr>
                                            <td>Cost of Vehicle ($):</td>
                                            <td><input type="text"  class="number cost" placeholder="10000" /></td>
                                        </tr>
                                        <tr>
                                            <td>Down Payment ($):</td>
                                            <td><input type="text"  class="number down_payment" placeholder="1000" /></td>
                                        </tr>
                                        <tr>
                                            <td>Annual Interest Rate (%):</td>
                                            <td><input type="text"  class="number interest" placeholder="7" /></td>
                                        </tr>
                                        <tr>
                                            <td>Term of Loan in Years:</td>
                                            <td><input type="text"  class="number loan_years" placeholder="5" /></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="bi_weekly clearfix">
                                <div class="pull-left">Frequency of Payments:</div>
                                <div class="dropdown_container ">
                                    <select class="frequency css-dropdowns">
                                        <option value='0'>Bi-Weekly</option>
                                        <option value='1'>Weekly</option>
                                        <option value='2'>Monthly</option>
                                    </select>
                                </div>
                            </div>
                            <a class="btn-inventory pull-right calculate">Calculate My Payment</a>
                            <div class="clear"></div>
                            <div class="calculation">
                                <div class="table-responsive">
                                    <table>
                                        <tr>
                                            <td><strong>NUMBER OF PAYMENTS:</strong></td>
                                            <td><strong class="payments">60</strong></td>
                                        </tr>
                                        <tr>
                                            <td><strong>PAYMENT AMOUNT:</strong></td>
                                            <td><strong class="payment_amount">$ 89.11</strong></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="recent-vehicles-wrap">
                <div class="row">
                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 recent-vehicles padding-left-none xs-padding-bottom-20">
                        <h5 class="margin-top-none">Similar Vehicles</h5>
                        <p>Browse through the vast
                            selection of vehicles that
                            have recently been added
                            to our inventory.</p>
                        <div class="arrow3 clearfix" id="slideControls3"><span class="prev-btn"></span><span class="next-btn"></span></div>
                    </div>
                    <div class="col-md-10 col-sm-8 padding-right-none sm-padding-left-none xs-padding-left-none">
                        <div class="carasouel-slider3">
                            <div class="slide">
                                <div class="car-block">
                                    <div class="img-flex"> <a href="#"><span class="align-center"><i class="fa fa-3x fa-plus-square-o"></i></span></a> <img src="{{ asset('images/c-car1.jpg') }}" alt="" class="img-responsive"> </div>
                                    <div class="car-block-bottom">
                                        <h6><strong>2012 Porsche Cayenne GTS</strong></h6>
                                        <h5>$ 79,995</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="slide">
                                <div class="car-block">
                                    <div class="img-flex"> <a href="#"><span class="align-center"><i class="fa fa-3x fa-plus-square-o"></i></span></a> <img src="{{ asset('images/c-car9.jpg') }}" alt="" class="img-responsive"> </div>
                                    <div class="car-block-bottom">
                                        <h6><strong>2009 Porsche Carrera 4S</strong></h6>
                                        <h5>$ 39,995</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!--container ends--> 
</section>
<!--content ends-->
<div class="clearfix"></div>
@endsection