@php    use Carbon\Carbon;  @endphp

@extends('layouts.site_master')
@section('content')
<section id="secondary-banner" class="dynamic-image-1"><!--for other images just change the class name of this section block like, class="dynamic-image-2" and add css for the changed class-->
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-8 col-sm-6 col-xs-12">
                <h2>Inventory</h2>
                <h4>Unlimited Listings, Any Vehicle Type</h4>
            </div>
            <div class="col-lg-6 col-md-4 col-sm-6 col-xs-12">
                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li>Inventory</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--secondary-banner ends-->
<div class="message-shadow"></div>
<div class="clearfix"></div>
<section class="content">
    <div class="container">
        <div class="inner-page row">
            <div class="listing-view margin-bottom-20 col-sm-12">
                <div class="row">
                    <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12 padding-none"> <span class="ribbon"><strong>{{-- 140  --}}Vehicles Matching:</strong></span> <span class="ribbon-item">All Listings</span> </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 padding-none pull-right select_view"> <span class="align-right">Select View:</span>
                        <ul class="page-view nav nav-tabs" id="myTab">
                            <li class="active"><a href="#full-width" data-toggle="tab"><i class="fa fa-align-left"></i></a></li>
                            <li><a href="#list-left-sidebar" data-toggle="tab"><i class="fa"></i></a></li>
                            <li><a href="#list-right-sidebar" data-toggle="tab"><i class="fa"></i></a></li>
                            <li><a href="#box-full-width" data-toggle="tab"><i class="fa"></i></a></li>
                            <li><a href="#left-box-fullwidth" data-toggle="tab"><i class="fa"></i></a></li>
                            <li><a href="#right-box-fullwidth" data-toggle="tab"><i class="fa"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <form method="post" action="#" class="listing_sort">
                <div class="select-wrapper listing_select clearfix margin-top-none margin-bottom-15 col-sm-12">
                    <div class="my-dropdown years-dropdown">
                        <select name="year" class="css-dropdowns" tabindex="1" >
                            <option value="">All Years</option>
                            @for ($i = $date_now; $i >= $date_before; $i--)
                                 <option>{{$i}}</option>
                            @endfor
                        </select>
                    </div>
                    <div class="my-dropdown makers-dropdown">
                        <select name="make" class="css-dropdowns" tabindex="1" >
                            <option value="">All Makes</option>
                            @foreach ($brends as $brend)
                                <option>{{$brend->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="my-dropdown models-dropdown">
                        <select name="model" class="css-dropdowns" tabindex="1" >
                            <option value="">All Models</option>
                            <option>Lorem</option>
                            <option>ipsum</option>
                            <option>dolor</option>
                            <option>sit</option>
                            <option>amet</option>
                        </select>
                    </div>
                    <div class="my-dropdown body-styles-dropdown">
                        <select name="body_style" class="css-dropdowns" tabindex="1" >
                            <option value="">All Body Styles</option>
                            @foreach ($body_styles as $body_style)
                                <option>{{$body_style->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="my-dropdown mileage-dropdown">
                        <select name="mileage" class="css-dropdowns" tabindex="1" >
                            <option value="">All Mileage</option>
                            <option>0</option>                  
                            @for ($i = 10000; $i <= 100000; $i=$i + 10000)
                                <option>&lt; {{number_format($i)}}</option>
                            @endfor
                            <option>&lt; 120,000</option>
                            <option>&lt; 150,000</option>
                        </select>
                    </div>
                    <div class="my-dropdown transmissions-dropdown">
                        <select name="transmission" class="css-dropdowns" tabindex="1" >
                            <option value="">All Transmissions</option>
                            <option>Automatic</option>
                            <option>Manual</option>
                        </select>
                    </div>
                    <div class="my-dropdown fuel-economies-dropdown">
                        <select name="fuel_economies" class="css-dropdowns" tabindex="1" >
                            <option value="">All Fuel Economies</option>
                            <option>10-20 MPG</option>
                            <option>20-30 MPG</option>
                            <option>30-40 MPG</option>
                            <option>40-50 MPG</option>
                            <option>50-60 MPG</option>
                        </select>
                    </div>
                    <div class="my-dropdown conditions-dropdown">
                        <select name="conditions" class="css-dropdowns" tabindex="1" >
                            <option value="">All Conditions</option>
                            <option>New</option>
                            <option>Used</option>
                        </select>
                    </div>
                    <div class="my-dropdown location-dropdown">
                        <select name="location" class="css-dropdowns" tabindex="1" >
                            <option value="">All Locations</option>
                            <option>Toronto</option>
                            <option>New York</option>
                            <option>Los Angeles</option>
                            <option>Vancouver</option>
                        </select>
                    </div>
                    <div class="my-dropdown prices-dropdown">
                        <select name="price" class="css-dropdowns" tabindex="1" >
                            <option value="">All Prices</option>
                            <option>&lt; $1,000</option>
                            @for ($i = 10000; $i <= 100000; $i=$i + 10000)
                                <option>&lt; {{'$'}}{{number_format($i)}}</option>
                            @endfor
                        </select>
                    </div>
                </div>
                {{-- <div class="select-wrapper pagination clearfix">
                    <div class="row">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 sort-by-menu"> <span class="sort-by">Sort By:</span>
                            <div class="my-dropdown price-ascending-dropdown">
                                <select name="price" class="css-dropdowns" tabindex="1" >
                                    <option value="">Price Ascending</option>
                                    <option>Ascending</option>
                                    <option>Descending</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 col-lg-offset-1">
                            <div class="controls full"> <a href="#" class="left-arrow"><i class="fa fa-angle-left"></i></a> <span>Page 1 of 4</span> <a href="#" class="right-arrow"><i class="fa fa-angle-right"></i></a> </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pull-right">
                            <ul class="form-links top_buttons">
                                <li><a href="#" class="gradient_button">Reset Filters</a></li>
                                <li><a href="#" class="gradient_button">Deselect All</a></li>
                                <li><a href="#" class="gradient_button">Compare 0 Vehicles</a></li>
                            </ul>
                        </div>
                    </div>
                </div> --}}
            </form>
            <div class="clearfix"></div>
            <div id="myTabContent" class="tab-content">
                <div class="tab-pane fade active in" id="full-width">
                    <div class="content-wrap">
                        <div class="row car_listings">
                            @foreach ($items as $item)
                                @php
                                    $time = $item->year;
                                    $date = new Carbon( $time ); 
                                @endphp
                                <div class="inventory margin-bottom-20 clearfix scroll_effect fadeIn">
                                    <input type="checkbox" name="a" class="checkbox compare_vehicle input-checkbox" id="1_vehicle_{{$item->id}}" />
                                    <label for="1_vehicle_{{$item->id}}"></label>
                                    <a class="inventory" href="{{ asset('/details/' . $item->id . '/') }}">
                                    <div class="title">{{$date->year}} {{$item->item_brend->name}} {{$item->item_model->name}} {{$item->item_body_style->name}}</div>
                                    <img src="images/car-1-200x150.jpg" class="preview" alt="preview">
                                    <table class="options-primary">
                                        @foreach ($technicals->where('item_detail_id', $item->id) as $technical)
                                            @php
                                                $tech[$technical->item_technical->name] = $technical->value;
                                            @endphp
                                        @endforeach
                                        <tr>
                                            <td class="option primary">Body Style:</td>
                                            <td class="spec">{{$item->item_body_style->name}}</td>
                                        </tr>
                                        <tr>
                                            <td class="option primary">Drivetrain:</td>
                                            <td class="spec">{{$tech['Drivetrain']}}</td>
                                        </tr>
                                        <tr>
                                            <td class="option primary">Engine:</td>
                                            <td class="spec">{{$tech['Displacement']}} {{$tech['Engine Layout']}} V{{$tech['Layout / number of cylinders']}}</td>
                                        </tr>
                                        <tr>
                                            <td class="option primary">Transmission:</td>
                                            <td class="spec">{{$tech['Manual Gearbox']}}</td>
                                        </tr>
                                        <tr>
                                            <td class="option primary">Mileage:</td>
                                            <td class="spec">{{$tech['Mileage']}}</td>
                                        </tr>
                                    </table>
                                    <table class="options-secondary">
                                        <tr>
                                            <td class="option secondary">Exterior Color:</td>
                                            <td class="spec">{{$tech['Exterior Color']}}</td>
                                        </tr>
                                        <tr>
                                            <td class="option secondary">Interior Color:</td>
                                            <td class="spec">{{$tech['Interior Color']}}</td>
                                        </tr>
                                        <tr>
                                            <td class="option secondary">MPG:</td>
                                            <td class="spec">{{$tech['City (estimate)']}} city / {{$tech['Highway (estimate)']}} hwy</td>
                                        </tr>
                                        <tr>
                                            <td class="option secondary">Stock Number:</td>
                                            <td class="spec">{{$item->id}}</td>
                                        </tr>
                                        <tr>
                                            <td class="option secondary">VIN Number:</td>
                                            <td class="spec">{{$tech['Vin #:']}}</td>
                                        </tr>
                                    </table>
                                    <img src="images/carfax.png" alt="carfax" class="carfax" />
                                    <div class="price"><b>Price:</b><br>
                                        <div class="figure">${{number_format($item->price)}}<br>
                                        </div>
                                        <div class="tax">Plus Sales Tax</div>
                                    </div>
                                    <div class="view-details gradient_button"><i class='fa fa-plus-circle'></i> View Details </div>
                                    <div class="clearfix"></div>
                                    </a>
                                    <div class="view-video gradient_button" data-youtube-id="3oh7PBc33dk"><i class="fa fa-video-camera"></i> View Video</div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <ul class="pagination margin-bottom-none margin-top-25">
                            <li class="disabled"><a href="#"><i class="fa fa-angle-left"></i></a></li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="tab-pane fade" id="list-left-sidebar">
                    <div class="row">
                        <div class="inventory-wide-sidebar-left">
                            <div class=" col-md-9 col-sm-12 col-lg-push-3 col-md-push-3">
                                <div class="sidebar car_listings margin-top-20 clearfix">
                                @foreach ($items as $item)
                                    @php
                                        $time = $item->year;
                                        $date = new Carbon( $time ); 
                                    @endphp
                                    <div class="inventory margin-bottom-20 clearfix scroll_effect fadeIn">
                                        <input type="checkbox" name="a" class="checkbox compare_vehicle input-checkbox" id="2_vehicle_{{$item->id}}" />
                                        <label for="2_vehicle_{{$item->id}}"></label>
                                        <a class="inventory" href="{{ asset('/details/' . $item->id . '/') }}">
                                        <div class="title">{{$date->year}} {{$item->item_brend->name}} {{$item->item_model->name}} {{$item->item_body_style->name}}</div>
                                        <img src="images/car-1-200x150.jpg" class="preview" alt="preview">
                                        <table class="options-primary">
                                            @foreach ($technicals->where('item_detail_id', $item->id) as $technical)
                                                @php
                                                    $tech[$technical->item_technical->name] = $technical->value;
                                                @endphp
                                            @endforeach
                                            <tr>
                                                <td class="option primary">Body Style:</td>
                                                <td class="spec">{{$item->item_body_style->name}}</td>
                                            </tr>
                                            <tr>
                                                <td class="option primary">Drivetrain:</td>
                                                <td class="spec">{{$tech['Drivetrain']}}</td>
                                            </tr>
                                            <tr>
                                                <td class="option primary">Engine:</td>
                                                <td class="spec">{{$tech['Displacement']}} {{$tech['Engine Layout']}} V{{$tech['Layout / number of cylinders']}}</td>
                                            </tr>
                                            <tr>
                                                <td class="option primary">Transmission:</td>
                                                <td class="spec">{{$tech['Manual Gearbox']}}</td>
                                            </tr>
                                            <tr>
                                                <td class="option primary">Mileage:</td>
                                                <td class="spec">{{$tech['Mileage']}}</td>
                                            </tr>
                                        </table>
                                        <table class="options-secondary">
                                            <tr>
                                                <td class="option secondary">Exterior Color:</td>
                                                <td class="spec">{{$tech['Exterior Color']}}</td>
                                            </tr>
                                            <tr>
                                                <td class="option secondary">Interior Color:</td>
                                                <td class="spec">{{$tech['Interior Color']}}</td>
                                            </tr>
                                            <tr>
                                                <td class="option secondary">MPG:</td>
                                                <td class="spec">{{$tech['City (estimate)']}} city / {{$tech['Highway (estimate)']}} hwy</td>
                                            </tr>
                                            <tr>
                                                <td class="option secondary">Stock Number:</td>
                                                <td class="spec">{{$item->id}}</td>
                                            </tr>
                                            <tr>
                                                <td class="option secondary">VIN Number:</td>
                                                <td class="spec">{{$tech['Vin #:']}}</td>
                                            </tr>
                                        </table>
                                        <img src="images/carfax.png" alt="carfax" class="carfax" />
                                        <div class="price"><b>Price:</b><br>
                                            <div class="figure">${{number_format($item->price)}}<br>
                                            </div>
                                            <div class="tax">Plus Sales Tax</div>
                                        </div>
                                        <div class="view-details gradient_button"><i class='fa fa-plus-circle'></i> View Details </div>
                                        <div class="clearfix"></div>
                                        </a>
                                        <div class="view-video gradient_button" data-youtube-id="3oh7PBc33dk"><i class="fa fa-video-camera"></i> View Video</div>
                                    </div>
                                @endforeach
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <ul class="pagination margin-bottom-none margin-top-25 md-margin-bottom-none xs-margin-bottom-60 sm-margin-bottom-60">
                                            <li class="disabled"><a href="#"><i class="fa fa-angle-left"></i></a></li>
                                            <li><a href="#">1</a></li>
                                            <li><a href="#">2</a></li>
                                            <li><a href="#">3</a></li>
                                            <li><a href="#">4</a></li>
                                            <li><a href="#">5</a></li>
                                            <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class=" col-md-3 col-sm-12 col-lg-pull-9 col-md-pull-9 left-sidebar">
                                <div class="left_inventory">
                                    <h3 class="margin-bottom-25">SEARCH OUR INVENTORY</h3>
                                    <form class="clearfix select-form padding-bottom-50">
                                        <div class="my-dropdown min-years-dropdown max-dropdown">
                                            <select name="year" class="css-dropdowns" tabindex="1" >
                                                <option value="">Search by Year</option>
                                                @for ($i = $date_now; $i >= $date_before; $i--)
                                                     <option>{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                        <div class="my-dropdown min-years-dropdown max-dropdown">
                                            <select name="make" class="css-dropdowns" tabindex="1" >
                                                <option value="">Search by Make</option>
                                                @foreach ($brends as $brend)
                                                    <option>{{$brend->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="my-dropdown min-years-dropdown max-dropdown">
                                            <select name="model" class="css-dropdowns" tabindex="1" >
                                                <option value="">Search by Model</option>
                                                <option>Lorem</option>
                                                <option>ipsum</option>
                                                <option>dolor</option>
                                                <option>sit</option>
                                                <option>amet</option>
                                            </select>
                                        </div>
                                        <div class="my-dropdown min-years-dropdown max-dropdown">
                                            <select name="body_style" class="css-dropdowns" tabindex="1" >
                                                <option value="">Search by Body Style</option>
                                                @foreach ($body_styles as $body_style)
                                                    <option>{{$body_style->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="my-dropdown min-years-dropdown max-dropdown">
                                            <select name="mileage" class="css-dropdowns" tabindex="1" >
                                                <option value="">Search by Mileage</option>
                                                <option>0</option>
                                                @for ($i = 10000; $i <= 100000; $i=$i + 10000)
                                                    <option>&lt; {{$i}}</option>
                                                @endfor
                                                <option>&lt; 120,000</option>
                                                <option>&lt; 150,000</option>
                                            </select>
                                        </div>
                                        <div class="my-dropdown min-years-dropdown max-dropdown">
                                            <select name="transmission" class="css-dropdowns" tabindex="1" >
                                                <option value="">Search by Transmission</option>
                                                <option>Automatic</option>
                                                <option>Manual</option>
                                            </select>
                                        </div>
                                        <div class="my-dropdown min-years-dropdown max-dropdown">
                                            <select name="fuel_economy" class="css-dropdowns" tabindex="1" >
                                                <option value="">Search by Fuel Economy</option>
                                                <option>10-20 MPG</option>
                                                <option>20-30 MPG</option>
                                                <option>30-40 MPG</option>
                                                <option>40-50 MPG</option>
                                                <option>50-60 MPG</option>
                                            </select>
                                        </div>
                                        <div class="my-dropdown min-years-dropdown max-dropdown">
                                            <select name="condition" class="css-dropdowns" tabindex="1" >
                                                <option value="">Search by Condition</option>
                                                <option>New</option>
                                                <option>Used</option>
                                            </select>
                                        </div>
                                        <div class="my-dropdown min-years-dropdown max-dropdown">
                                            <select name="location" class="css-dropdowns" tabindex="1" >
                                                <option value="">Search by Location</option>
                                                <option>Toronto</option>
                                                <option>New York</option>
                                                <option>Los Angeles</option>
                                                <option>Vancouver</option>
                                            </select>
                                        </div>
                                        <div class="my-dropdown min-years-dropdown max-dropdown">
                                            <select name="price" class="css-dropdowns" tabindex="1" >
                                                <option value="">Search by Price</option>
                                                <option>&lt; $1,000</option>
                                                @for ($i = 10000; $i <= 100000; $i=$i + 10000)
                                                    <option>&lt; {{'$'}}{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                        <input type="reset" value="Reset Search Filters" class="pull-left btn-inventory margin-bottom-none md-button" />
                                    </form>
                                    <div class="side-content">
                                        <div class="list col-md-12 col-sm-3 padding-bottom-50">
                                            <h3 class="margin-bottom-25">YEAR</h3>
                                            <ul>
                                                @for ($i = $date_now; $i >= $date_before + 10; $i--)
                                                    <li><a href="#"><span>{{$i}}<strong>(
                                                    {{$date->year}}



                                                    28)</strong></span></a></li>
                                                @endfor
                                                <li><a href="#">View More...</a></li>
                                            </ul>
                                        </div>
                                        <div class="list col-md-12 col-sm-3 padding-bottom-50">
                                            <h3 class="margin-bottom-25">MAKE</h3>
                                            <ul>
                                                <li><a href="#"><span>Acura <strong>(2)</strong></span></a></li>
                                                <li><a href="#"><span>BMW <strong>(4)</strong></span></a></li>
                                                <li><a href="#"><span>Buick <strong>(1)</strong></span></a></li>
                                                <li><a href="#"><span>Cadillac <strong>(6)</strong></span></a></li>
                                                <li><a href="#"><span>Chevrolet <strong>(19)</strong></span></a></li>
                                                <li><a href="#"><span>Chrysler <strong>(7)</strong></span></a></li>
                                                <li><a href="#"><span>Dodge <strong>(14)</strong></span></a></li>
                                                <li><a href="#"><span>Ford <strong>(15)</strong></span></a></li>
                                                <li><a href="#"><span>GMC <strong>(9)</strong></span></a></li>
                                                <li><a href="#"><span>Hummer <strong>(3)</strong></span></a></li>
                                                <li><a href="#">View More...</a></li>
                                            </ul>
                                        </div>
                                        <div class="financing_calculator col-md-12 col-sm-6">
                                            <h3 class="margin-bottom-25">FINANCING CALCULATOR</h3>
                                            <div class="table-responsive">
                                                <table class="table no-border no-margin">
                                                    <tbody>
                                                        <tr>
                                                            <td>Cost of Vehicle ($):</td>
                                                            <td><input type="text"  class="number cost" placeholder="10000" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Down Payment ($):</td>
                                                            <td><input type="text"  class="number down_payment" placeholder="1000" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Annual Interest Rate (%):</td>
                                                            <td><input type="text"  class="number interest" placeholder="7" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Term of Loan in Years:</td>
                                                            <td><input type="text"  class="number loan_years" placeholder="5" /></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="bi_weekly clearfix">
                                                <div class="pull-left">Frequency of Payments:</div>
                                                <div class="dropdown_container">
                                                    <select class="frequency css-dropdowns">
                                                        <option value='0'>Bi-Weekly</option>
                                                        <option value='1'>Weekly</option>
                                                        <option value='2'>Monthly</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <a class="btn-inventory pull-right calculate">Calculate My Payment</a>
                                            <div class="clear"></div>
                                            <div class="table-responsive">
                                                <table>
                                                    <tr>
                                                        <td><strong>NUMBER OF PAYMENTS:</strong></td>
                                                        <td><strong>60</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong>PAYMENT AMOUNT:</strong></td>
                                                        <td><strong>$ 89.11</strong></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="list-right-sidebar">
                    <div class="row">
                        <div class="inventory-wide-sidebar-right">
                            <div class="col-md-9">
                                <div class="sidebar car_listings margin-top-20 clearfix">
                                @foreach ($items as $item)
                                    @php
                                        $time = $item->year;
                                        $date = new Carbon( $time ); 
                                    @endphp
                                    <div class="inventory margin-bottom-20 clearfix scroll_effect fadeIn">
                                        <input type="checkbox" name="a" class="checkbox compare_vehicle input-checkbox" id="3_vehicle_{{$item->id}}" />
                                        <label for="3_vehicle_{{$item->id}}"></label>
                                        <a class="inventory" href="{{ asset('/details/' . $item->id . '/') }}">
                                        <div class="title">{{$date->year}} {{$item->item_brend->name}} {{$item->item_model->name}} {{$item->item_body_style->name}}</div>
                                        <img src="images/car-1-200x150.jpg" class="preview" alt="preview">
                                        <table class="options-primary">
                                            @foreach ($technicals->where('item_detail_id', $item->id) as $technical)
                                                @php
                                                    $tech[$technical->item_technical->name] = $technical->value;
                                                @endphp
                                            @endforeach
                                            <tr>
                                                <td class="option primary">Body Style:</td>
                                                <td class="spec">{{$item->item_body_style->name}}</td>
                                            </tr>
                                            <tr>
                                                <td class="option primary">Drivetrain:</td>
                                                <td class="spec">{{$tech['Drivetrain']}}</td>
                                            </tr>
                                            <tr>
                                                <td class="option primary">Engine:</td>
                                                <td class="spec">{{$tech['Displacement']}} {{$tech['Engine Layout']}} V{{$tech['Layout / number of cylinders']}}</td>
                                            </tr>
                                            <tr>
                                                <td class="option primary">Transmission:</td>
                                                <td class="spec">{{$tech['Manual Gearbox']}}</td>
                                            </tr>
                                            <tr>
                                                <td class="option primary">Mileage:</td>
                                                <td class="spec">{{$tech['Mileage']}}</td>
                                            </tr>
                                        </table>
                                        <table class="options-secondary">
                                            <tr>
                                                <td class="option secondary">Exterior Color:</td>
                                                <td class="spec">{{$tech['Exterior Color']}}</td>
                                            </tr>
                                            <tr>
                                                <td class="option secondary">Interior Color:</td>
                                                <td class="spec">{{$tech['Interior Color']}}</td>
                                            </tr>
                                            <tr>
                                                <td class="option secondary">MPG:</td>
                                                <td class="spec">{{$tech['City (estimate)']}} city / {{$tech['Highway (estimate)']}} hwy</td>
                                            </tr>
                                            <tr>
                                                <td class="option secondary">Stock Number:</td>
                                                <td class="spec">{{$item->id}}</td>
                                            </tr>
                                            <tr>
                                                <td class="option secondary">VIN Number:</td>
                                                <td class="spec">{{$tech['Vin #:']}}</td>
                                            </tr>
                                        </table>
                                        <img src="images/carfax.png" alt="carfax" class="carfax" />
                                        <div class="price"><b>Price:</b><br>
                                            <div class="figure">${{number_format($item->price)}}<br>
                                            </div>
                                            <div class="tax">Plus Sales Tax</div>
                                        </div>
                                        <div class="view-details gradient_button"><i class='fa fa-plus-circle'></i> View Details </div>
                                        <div class="clearfix"></div>
                                        </a>
                                        <div class="view-video gradient_button" data-youtube-id="3oh7PBc33dk"><i class="fa fa-video-camera"></i> View Video</div>
                                    </div>
                                @endforeach
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <ul class="pagination margin-bottom-none margin-top-25 md-margin-bottom-none xs-margin-bottom-60 sm-margin-bottom-60">
                                            <li class="disabled"><a href="#"><i class="fa fa-angle-left"></i></a></li>
                                            <li><a href="#">1</a></li>
                                            <li><a href="#">2</a></li>
                                            <li><a href="#">3</a></li>
                                            <li><a href="#">4</a></li>
                                            <li><a href="#">5</a></li>
                                            <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="inventory-sidebar col-md-3">
                                <div class="left_inventory">
                                    <h3 class="margin-bottom-25">SEARCH OUR INVENTORY</h3>
                                    <form class="clearfix select-form padding-bottom-50">
                                        <div class="my-dropdown min-years-dropdown max-dropdown">
                                            <select name="year" class="css-dropdowns" tabindex="1" >
                                                <option value="">Search by Year</option>
                                                @for ($i = $date_now; $i >= $date_before; $i--)
                                                     <option>{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                        <div class="my-dropdown min-years-dropdown max-dropdown">
                                            <select name="make" class="css-dropdowns" tabindex="1" >
                                                <option value="">Search by Make</option>
                                                @foreach ($brends as $brend)
                                                    <option>{{$brend->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="my-dropdown min-years-dropdown max-dropdown">
                                            <select name="model" class="css-dropdowns" tabindex="1" >
                                                <option value="">Search by Model</option>
                                                <option>Lorem</option>
                                                <option>ipsum</option>
                                                <option>dolor</option>
                                                <option>sit</option>
                                                <option>amet</option>
                                            </select>
                                        </div>
                                        <div class="my-dropdown min-years-dropdown max-dropdown">
                                            <select name="body_style" class="css-dropdowns" tabindex="1" >
                                                <option value="">Search by Body Style</option>
                                                @foreach ($body_styles as $body_style)
                                                    <option>{{$body_style->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="my-dropdown min-years-dropdown max-dropdown">
                                            <select name="mileage" class="css-dropdowns" tabindex="1" >
                                                <option value="">Search by Mileage</option>
                                                <option>0</option>
                                                @for ($i = 10000; $i <= 100000; $i=$i + 10000)
                                                    <option>&lt; {{$i}}</option>
                                                @endfor
                                                <option>&lt; 120,000</option>
                                                <option>&lt; 150,000</option>
                                            </select>
                                        </div>
                                        <div class="my-dropdown min-years-dropdown max-dropdown">
                                            <select name="transmission" class="css-dropdowns" tabindex="1" >
                                                <option value="">Search by Transmission</option>
                                                <option>Automatic</option>
                                                <option>Manual</option>
                                            </select>
                                        </div>
                                        <div class="my-dropdown min-years-dropdown max-dropdown">
                                            <select name="fuel_economy" class="css-dropdowns" tabindex="1" >
                                                <option value="">Search by Fuel Economy</option>
                                                <option>10-20 MPG</option>
                                                <option>20-30 MPG</option>
                                                <option>30-40 MPG</option>
                                                <option>40-50 MPG</option>
                                                <option>50-60 MPG</option>
                                            </select>
                                        </div>
                                        <div class="my-dropdown min-years-dropdown max-dropdown">
                                            <select name="condition" class="css-dropdowns" tabindex="1" >
                                                <option value="">Search by Condition</option>
                                                <option>New</option>
                                                <option>Used</option>
                                            </select>
                                        </div>
                                        <div class="my-dropdown min-years-dropdown max-dropdown">
                                            <select name="location" class="css-dropdowns" tabindex="1" >
                                                <option value="">Search by Location</option>
                                                <option>Toronto</option>
                                                <option>New York</option>
                                                <option>Los Angeles</option>
                                                <option>Vancouver</option>
                                            </select>
                                        </div>
                                        <div class="my-dropdown min-years-dropdown max-dropdown">
                                            <select name="price" class="css-dropdowns" tabindex="1" >
                                                <option value="">Search by Price</option>
                                                <option>&lt; $1,000</option>
                                                @for ($i = 10000; $i <= 100000; $i=$i + 10000)
                                                    <option>&lt; {{'$'}}{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                        <input type="reset" value="Reset Search Filters" class="pull-left btn-inventory margin-bottom-none md-button" />
                                    </form>
                                    <div class="side-content">
                                        <div class="list col-lg-12 col-md-12 col-sm-3 padding-bottom-50">
                                            <h3 class="margin-bottom-25">YEAR</h3>
                                            <ul>
                                                @for ($i = $date_now; $i >= $date_before + 10; $i--)
                                                    <li><a href="#"><span>{{$i}}<strong>(28)</strong></span></a></li>
                                                @endfor
                                                <li><a href="#">View More...</a></li>
                                            </ul>
                                        </div>
                                        <div class="list col-lg-12 col-md-12 col-sm-3 padding-bottom-50">
                                            <h3 class="margin-bottom-25">MAKE</h3>
                                            <ul>
                                                <li><a href="#"><span>Acura <strong>(2)</strong></span></a></li>
                                                <li><a href="#"><span>BMW <strong>(4)</strong></span></a></li>
                                                <li><a href="#"><span>Buick <strong>(1)</strong></span></a></li>
                                                <li><a href="#"><span>Cadillac <strong>(6)</strong></span></a></li>
                                                <li><a href="#"><span>Chevrolet <strong>(19)</strong></span></a></li>
                                                <li><a href="#"><span>Chrysler <strong>(7)</strong></span></a></li>
                                                <li><a href="#"><span>Dodge <strong>(14)</strong></span></a></li>
                                                <li><a href="#"><span>Ford <strong>(15)</strong></span></a></li>
                                                <li><a href="#"><span>GMC <strong>(9)</strong></span></a></li>
                                                <li><a href="#"><span>Hummer <strong>(3)</strong></span></a></li>
                                                <li><a href="#">View More...</a></li>
                                            </ul>
                                        </div>
                                        <div class="financing_calculator col-lg-12 col-md-12 col-sm-6">
                                            <h3 class="margin-bottom-25">FINANCING CALCULATOR</h3>
                                            <div class="table-responsive">
                                                <table class="table no-border no-margin">
                                                    <tbody>
                                                        <tr>
                                                            <td>Cost of Vehicle ($):</td>
                                                            <td><input type="text"  class="number cost" placeholder="10000" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Down Payment ($):</td>
                                                            <td><input type="text"  class="number down_payment" placeholder="1000" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Annual Interest Rate (%):</td>
                                                            <td><input type="text"  class="number interest" placeholder="7" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Term of Loan in Years:</td>
                                                            <td><input type="text"  class="number loan_years" placeholder="5" /></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="bi_weekly clearfix">
                                                <div class="pull-left">Frequency of Payments:</div>
                                                <div class="dropdown_container">
                                                    <select class="frequency css-dropdowns">
                                                        <option value='0'>Bi-Weekly</option>
                                                        <option value='1'>Weekly</option>
                                                        <option value='2'>Monthly</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <a class="btn-inventory pull-right calculate">Calculate My Payment</a>
                                            <div class="clear"></div>
                                            <div class="table-responsive">
                                                <table>
                                                    <tr>
                                                        <td><strong>NUMBER OF PAYMENTS:</strong></td>
                                                        <td><strong>60</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong>PAYMENT AMOUNT:</strong></td>
                                                        <td><strong>$ 89.11</strong></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="box-full-width">
                    <div class="inventory_box car_listings boxed boxed_full">
                        <div class="vehicle_demo clearfix">
                        @foreach ($items as $item)
                            @php
                                $time = $item->year;
                                $date = new Carbon( $time ); 
                            @endphp
                            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <div class="inventory margin-bottom-20 clearfix scroll_effect fadeIn">
                                    <input type="checkbox" name="a" class="checkbox compare_vehicle input-checkbox" id="4_vehicle_{{$item->id}}" />
                                    <label for="4_vehicle_{{$item->id}}"></label>
                                    <a class="inventory" href="{{ asset('/details/' . $item->id . '/') }}">
                                    <div class="title">{{$date->year}} {{$item->item_brend->name}} {{$item->item_model->name}} {{$item->item_body_style->name}}</div>
                                    <img src="images/car-1-200x150.jpg" class="preview" alt="preview">
                                    <table class="options-primary">
                                        @foreach ($technicals->where('item_detail_id', $item->id) as $technical)
                                            @php
                                                $tech[$technical->item_technical->name] = $technical->value;
                                            @endphp
                                        @endforeach
                                        <tr>
                                            <td class="option primary">Body Style:</td>
                                            <td class="spec">{{$item->item_body_style->name}}</td>
                                        </tr>
                                        <tr>
                                            <td class="option primary">Drivetrain:</td>
                                            <td class="spec">{{$tech['Drivetrain']}}</td>
                                        </tr>
                                        <tr>
                                            <td class="option primary">Engine:</td>
                                            <td class="spec">{{$tech['Displacement']}} {{$tech['Engine Layout']}} V{{$tech['Layout / number of cylinders']}}</td>
                                        </tr>
                                        <tr>
                                            <td class="option primary">Transmission:</td>
                                            <td class="spec">{{$tech['Manual Gearbox']}}</td>
                                        </tr>
                                        <tr>
                                            <td class="option primary">Mileage:</td>
                                            <td class="spec">{{$tech['Mileage']}}</td>
                                        </tr>
                                    </table>
                                    <table class="options-secondary">
                                        <tr>
                                            <td class="option secondary">Exterior Color:</td>
                                            <td class="spec">{{$tech['Exterior Color']}}</td>
                                        </tr>
                                        <tr>
                                            <td class="option secondary">Interior Color:</td>
                                            <td class="spec">{{$tech['Interior Color']}}</td>
                                        </tr>
                                        <tr>
                                            <td class="option secondary">MPG:</td>
                                            <td class="spec">{{$tech['City (estimate)']}} city / {{$tech['Highway (estimate)']}} hwy</td>
                                        </tr>
                                        <tr>
                                            <td class="option secondary">Stock Number:</td>
                                            <td class="spec">{{$item->id}}</td>
                                        </tr>
                                        <tr>
                                            <td class="option secondary">VIN Number:</td>
                                            <td class="spec">{{$tech['Vin #:']}}</td>
                                        </tr>
                                    </table>
                                    <img src="images/carfax.png" alt="carfax" class="carfax" />
                                    <div class="price"><b>Price:</b><br>
                                        <div class="figure">${{number_format($item->price)}}<br>
                                        </div>
                                        <div class="tax">Plus Sales Tax</div>
                                    </div>
                                    <div class="view-details gradient_button"><i class='fa fa-plus-circle'></i> View Details </div>
                                    <div class="clearfix"></div>
                                    </a>
                                    <div class="view-video gradient_button" data-youtube-id="3oh7PBc33dk"><i class="fa fa-video-camera"></i> View Video</div>
                                </div>
                            </div>
                        @endforeach
                        </div>
                        <div class="inventory_pagination">
                            <ul class="pagination margin-bottom-none margin-top-25">
                                <li class="disabled"><a href="#" class="disabled"><i class="fa fa-angle-left"></i></a></li>
                                <li><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="tab-pane fade" id="left-box-fullwidth">
                    <div class="row">
                        <div class="car_listings boxed boxed_left col-md-9 col-lg-push-3 col-md-push-3">
                            <div class="inventory_box">
                                <div class="row clearfix">
                                    <div class="clearfix"></div>
                                @foreach ($items as $item)
                                    @php
                                        $time = $item->year;
                                        $date = new Carbon( $time ); 
                                    @endphp
                                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                        <div class="inventory margin-bottom-20 clearfix scroll_effect fadeIn">
                                            <input type="checkbox" name="a" class="checkbox compare_vehicle input-checkbox" id="5_vehicle_{{$item->id}}" />
                                            <label for="5_vehicle_{{$item->id}}"></label>
                                            <a class="inventory" href="{{ asset('/details/' . $item->id . '/') }}">
                                            <div class="title">{{$date->year}} {{$item->item_brend->name}} {{$item->item_model->name}} {{$item->item_body_style->name}}</div>
                                            <img src="images/car-1-200x150.jpg" class="preview" alt="preview">
                                            <table class="options-primary">
                                                @foreach ($technicals->where('item_detail_id', $item->id) as $technical)
                                                    @php
                                                        $tech[$technical->item_technical->name] = $technical->value;
                                                    @endphp
                                                @endforeach
                                                <tr>
                                                    <td class="option primary">Body Style:</td>
                                                    <td class="spec">{{$item->item_body_style->name}}</td>
                                                </tr>
                                                <tr>
                                                    <td class="option primary">Drivetrain:</td>
                                                    <td class="spec">{{$tech['Drivetrain']}}</td>
                                                </tr>
                                                <tr>
                                                    <td class="option primary">Engine:</td>
                                                    <td class="spec">{{$tech['Displacement']}} {{$tech['Engine Layout']}} V{{$tech['Layout / number of cylinders']}}</td>
                                                </tr>
                                                <tr>
                                                    <td class="option primary">Transmission:</td>
                                                    <td class="spec">{{$tech['Manual Gearbox']}}</td>
                                                </tr>
                                                <tr>
                                                    <td class="option primary">Mileage:</td>
                                                    <td class="spec">{{$tech['Mileage']}}</td>
                                                </tr>
                                            </table>
                                            <table class="options-secondary">
                                                <tr>
                                                    <td class="option secondary">Exterior Color:</td>
                                                    <td class="spec">{{$tech['Exterior Color']}}</td>
                                                </tr>
                                                <tr>
                                                    <td class="option secondary">Interior Color:</td>
                                                    <td class="spec">{{$tech['Interior Color']}}</td>
                                                </tr>
                                                <tr>
                                                    <td class="option secondary">MPG:</td>
                                                    <td class="spec">{{$tech['City (estimate)']}} city / {{$tech['Highway (estimate)']}} hwy</td>
                                                </tr>
                                                <tr>
                                                    <td class="option secondary">Stock Number:</td>
                                                    <td class="spec">{{$item->id}}</td>
                                                </tr>
                                                <tr>
                                                    <td class="option secondary">VIN Number:</td>
                                                    <td class="spec">{{$tech['Vin #:']}}</td>
                                                </tr>
                                            </table>
                                            <img src="images/carfax.png" alt="carfax" class="carfax" />
                                            <div class="price"><b>Price:</b><br>
                                                <div class="figure">${{number_format($item->price)}}<br>
                                                </div>
                                                <div class="tax">Plus Sales Tax</div>
                                            </div>
                                            <div class="view-details gradient_button"><i class='fa fa-plus-circle'></i> View Details </div>
                                            <div class="clearfix"></div>
                                            </a>
                                            <div class="view-video gradient_button" data-youtube-id="3oh7PBc33dk"><i class="fa fa-video-camera"></i> View Video</div>
                                        </div>
                                    </div>
                                @endforeach
                                </div>
                            </div>
                            <div class="inventory_pagination md-margin-bottom-none xs-margin-bottom-60 sm-margin-bottom-60">
                                <ul class="pagination margin-bottom-none margin-top-25">
                                    <li class="disabled"><a href="#"><i class="fa fa-angle-left"></i></a></li>
                                    <li><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#">5</a></li>
                                    <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="inventory-sidebar col-md-3 col-sm-12 col-lg-pull-9 col-md-pull-9 left-sidebar">
                            <div class="left_inventory">
                                <h3 class="margin-bottom-25">SEARCH OUR INVENTORY</h3>
                                <form class="clearfix select-form padding-bottom-50">
                                    <div class="my-dropdown min-years-dropdown max-dropdown">
                                        <select name="year" class="css-dropdowns" tabindex="1" >
                                            <option value="">Search by Year</option>
                                                @for ($i = $date_now; $i >= $date_before; $i--)
                                                     <option>{{$i}}</option>
                                                @endfor
                                        </select>
                                    </div>
                                    <div class="my-dropdown min-years-dropdown max-dropdown">
                                        <select name="make" class="css-dropdowns" tabindex="1" >
                                            <option value="">Search by Make</option>
                                                @foreach ($brends as $brend)
                                                    <option>{{$brend->name}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                    <div class="my-dropdown min-years-dropdown max-dropdown">
                                        <select name="model" class="css-dropdowns" tabindex="1" >
                                            <option value="">Search by Model</option>
                                            <option>Lorem</option>
                                            <option>ipsum</option>
                                            <option>dolor</option>
                                            <option>sit</option>
                                            <option>amet</option>
                                        </select>
                                    </div>
                                    <div class="my-dropdown min-years-dropdown max-dropdown">
                                        <select name="body_style" class="css-dropdowns" tabindex="1" >
                                            <option value="">Search by Body Style</option>
                                                @foreach ($body_styles as $body_style)
                                                    <option>{{$body_style->name}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                    <div class="my-dropdown min-years-dropdown max-dropdown">
                                        <select name="mileage" class="css-dropdowns" tabindex="1" >
                                            <option value="">Search by Mileage</option>
                                            <option>0</option>
                                                @for ($i = 10000; $i <= 100000; $i=$i + 10000)
                                                    <option>&lt; {{$i}}</option>
                                                @endfor
                                            <option>&lt; 120,000</option>
                                            <option>&lt; 150,000</option>
                                        </select>
                                    </div>
                                    <div class="my-dropdown min-years-dropdown max-dropdown">
                                        <select name="transmission" class="css-dropdowns" tabindex="1" >
                                            <option value="">Search by Transmission</option>
                                            <option>Automatic</option>
                                            <option>Manual</option>
                                        </select>
                                    </div>
                                    <div class="my-dropdown min-years-dropdown max-dropdown">
                                        <select name="fuel_economy" class="css-dropdowns" tabindex="1" >
                                            <option value="">Search by Fuel Economy</option>
                                            <option>10-20 MPG</option>
                                            <option>20-30 MPG</option>
                                            <option>30-40 MPG</option>
                                            <option>40-50 MPG</option>
                                            <option>50-60 MPG</option>
                                        </select>
                                    </div>
                                    <div class="my-dropdown min-years-dropdown max-dropdown">
                                        <select name="condition" class="css-dropdowns" tabindex="1" >
                                            <option value="">Search by Condition</option>
                                            <option>New</option>
                                            <option>Used</option>
                                        </select>
                                    </div>
                                    <div class="my-dropdown min-years-dropdown max-dropdown">
                                        <select name="location" class="css-dropdowns" tabindex="1" >
                                            <option value="">Search by Location</option>
                                            <option>Toronto</option>
                                            <option>New York</option>
                                            <option>Los Angeles</option>
                                            <option>Vancouver</option>
                                        </select>
                                    </div>
                                    <div class="my-dropdown min-years-dropdown max-dropdown">
                                        <select name="price" class="css-dropdowns" tabindex="1" >
                                            <option value="">Search by Price</option>
                                            <option>&lt; $1,000</option>
                                                @for ($i = 10000; $i <= 100000; $i=$i + 10000)
                                                    <option>&lt; {{'$'}}{{$i}}</option>
                                                @endfor
                                        </select>
                                    </div>
                                    <input type="reset" value="Reset Search Filters" class="pull-left btn-inventory margin-bottom-none md-button" />
                                </form>
                                <div class="side-content">
                                    <div class="list col-md-12 col-sm-3 padding-bottom-50">
                                        <h3 class="margin-bottom-25">YEAR</h3>
                                        <ul>
                                                @for ($i = $date_now; $i >= $date_before + 10; $i--)
                                                    <li><a href="#"><span>{{$i}}<strong>(28)</strong></span></a></li>
                                                @endfor
                                            <li><a href="#">View More...</a></li>
                                        </ul>
                                    </div>
                                    <div class="list col-md-12 col-sm-3 padding-bottom-50">
                                        <h3 class="margin-bottom-25">MAKE</h3>
                                        <ul>
                                            <li><span>Acura <strong>(2)</strong></span></li>
                                            <li><span>BMW <strong>(4)</strong></span></li>
                                            <li><span>Buick <strong>(1)</strong></span></li>
                                            <li><span>Cadillac <strong>(6)</strong></span></li>
                                            <li><span>Chevrolet <strong>(19)</strong></span></li>
                                            <li><span>Chrysler <strong>(7)</strong></span></li>
                                            <li><span>Dodge <strong>(14)</strong></span></li>
                                            <li><span>Ford <strong>(15)</strong></span></li>
                                            <li><span>GMC <strong>(9)</strong></span></li>
                                            <li><span>Hummer <strong>(3)</strong></span></li>
                                            <li><a href="#">View More...</a></li>
                                        </ul>
                                    </div>
                                    <div class="financing_calculator col-md-12 col-sm-6">
                                        <h3 class="margin-bottom-25">FINANCING CALCULATOR</h3>
                                        <div class="table-responsive">
                                            <table class="table no-border no-margin">
                                                <tbody>
                                                    <tr>
                                                        <td>Cost of Vehicle ($):</td>
                                                        <td><input type="text"  class="number cost" placeholder="10000" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Down Payment ($):</td>
                                                        <td><input type="text"  class="number down_payment" placeholder="1000" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Annual Interest Rate (%):</td>
                                                        <td><input type="text"  class="number interest" placeholder="7" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Term of Loan in Years:</td>
                                                        <td><input type="text"  class="number loan_years" placeholder="5" /></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="bi_weekly clearfix">
                                            <div class="pull-left">Frequency of Payments:</div>
                                            <div class="dropdown_container">
                                                <select class="frequency css-dropdowns">
                                                    <option value='0'>Bi-Weekly</option>
                                                    <option value='1'>Weekly</option>
                                                    <option value='2'>Monthly</option>
                                                </select>
                                            </div>
                                        </div>
                                        <a class="btn-inventory pull-right calculate">Calculate My Payment</a>
                                        <div class="clear"></div>
                                        <div class="table-responsive">
                                            <table>
                                                <tr>
                                                    <td><strong>NUMBER OF PAYMENTS:</strong></td>
                                                    <td><strong class="payments">60</strong></td>
                                                </tr>
                                                <tr>
                                                    <td><strong>PAYMENT AMOUNT:</strong></td>
                                                    <td><strong class="payment_amount">$ 89.11</strong></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="tab-pane fade" id="right-box-fullwidth">
                    <div class="row">
                        <div class="car_listings boxed boxed_right col-md-9">
                            <div class="inventory_box">
                                <div class="row clearfix">
                                @foreach ($items as $item)
                                    @php
                                        $time = $item->year;
                                        $date = new Carbon( $time ); 
                                    @endphp
                                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                        <div class="inventory margin-bottom-20 clearfix scroll_effect fadeIn">
                                            <input type="checkbox" name="a" class="checkbox compare_vehicle input-checkbox" id="6_vehicle_{{$item->id}}" />
                                            <label for="6_vehicle_{{$item->id}}"></label>
                                            <a class="inventory" href="{{ asset('/details/' . $item->id . '/') }}">
                                            <div class="title">{{$date->year}} {{$item->item_brend->name}} {{$item->item_model->name}} {{$item->item_body_style->name}}</div>
                                            <img src="images/car-1-200x150.jpg" class="preview" alt="preview">
                                            <table class="options-primary">
                                                @foreach ($technicals->where('item_detail_id', $item->id) as $technical)
                                                    @php
                                                        $tech[$technical->item_technical->name] = $technical->value;
                                                    @endphp
                                                @endforeach
                                                <tr>
                                                    <td class="option primary">Body Style:</td>
                                                    <td class="spec">{{$item->item_body_style->name}}</td>
                                                </tr>
                                                <tr>
                                                    <td class="option primary">Drivetrain:</td>
                                                    <td class="spec">{{$tech['Drivetrain']}}</td>
                                                </tr>
                                                <tr>
                                                    <td class="option primary">Engine:</td>
                                                    <td class="spec">{{$tech['Displacement']}} {{$tech['Engine Layout']}} V{{$tech['Layout / number of cylinders']}}</td>
                                                </tr>
                                                <tr>
                                                    <td class="option primary">Transmission:</td>
                                                    <td class="spec">{{$tech['Manual Gearbox']}}</td>
                                                </tr>
                                                <tr>
                                                    <td class="option primary">Mileage:</td>
                                                    <td class="spec">{{$tech['Mileage']}}</td>
                                                </tr>
                                            </table>
                                            <table class="options-secondary">
                                                <tr>
                                                    <td class="option secondary">Exterior Color:</td>
                                                    <td class="spec">{{$tech['Exterior Color']}}</td>
                                                </tr>
                                                <tr>
                                                    <td class="option secondary">Interior Color:</td>
                                                    <td class="spec">{{$tech['Interior Color']}}</td>
                                                </tr>
                                                <tr>
                                                    <td class="option secondary">MPG:</td>
                                                    <td class="spec">{{$tech['City (estimate)']}} city / {{$tech['Highway (estimate)']}} hwy</td>
                                                </tr>
                                                <tr>
                                                    <td class="option secondary">Stock Number:</td>
                                                    <td class="spec">{{$item->id}}</td>
                                                </tr>
                                                <tr>
                                                    <td class="option secondary">VIN Number:</td>
                                                    <td class="spec">{{$tech['Vin #:']}}</td>
                                                </tr>
                                            </table>
                                            <img src="images/carfax.png" alt="carfax" class="carfax" />
                                            <div class="price"><b>Price:</b><br>
                                                <div class="figure">${{number_format($item->price)}}<br>
                                                </div>
                                                <div class="tax">Plus Sales Tax</div>
                                            </div>
                                            <div class="view-details gradient_button"><i class='fa fa-plus-circle'></i> View Details </div>
                                            <div class="clearfix"></div>
                                            </a>
                                            <div class="view-video gradient_button" data-youtube-id="3oh7PBc33dk"><i class="fa fa-video-camera"></i> View Video</div>
                                        </div>
                                    </div>
                                @endforeach
                                </div>
                            </div>
                            <div class="inventory_pagination md-margin-bottom-none xs-margin-bottom-60 sm-margin-bottom-60">
                                <ul class="pagination margin-bottom-none margin-top-25">
                                    <li class="disabled"><a href="#"><i class="fa fa-angle-left"></i></a></li>
                                    <li><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#">5</a></li>
                                    <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="inventory-sidebar col-md-3">
                            <div class="left_inventory">
                                <h3 class="margin-bottom-25">SEARCH OUR INVENTORY</h3>
                                <form class="clearfix select-form padding-bottom-50">
                                    <div class="my-dropdown min-years-dropdown max-dropdown">
                                        <select name="year" class="css-dropdowns" tabindex="1" >
                                            <option value="">Search by Year</option>
                                                @for ($i = $date_now; $i >= $date_before; $i--)
                                                     <option>{{$i}}</option>
                                                @endfor
                                        </select>
                                    </div>
                                    <div class="my-dropdown min-years-dropdown max-dropdown">
                                        <select name="make" class="css-dropdowns" tabindex="1" >
                                            <option value="">Search by Make</option>
                                                @foreach ($brends as $brend)
                                                    <option>{{$brend->name}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                    <div class="my-dropdown min-years-dropdown max-dropdown">
                                        <select name="model" class="css-dropdowns" tabindex="1" >
                                            <option value="">Search by Model</option>
                                            <option>Lorem</option>
                                            <option>ipsum</option>
                                            <option>dolor</option>
                                            <option>sit</option>
                                            <option>amet</option>
                                        </select>
                                    </div>
                                    <div class="my-dropdown min-years-dropdown max-dropdown">
                                        <select name="body_style" class="css-dropdowns" tabindex="1" >
                                            <option value="">Search by Body Style</option>
                                                @foreach ($body_styles as $body_style)
                                                    <option>{{$body_style->name}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                    <div class="my-dropdown min-years-dropdown max-dropdown">
                                        <select name="mileage" class="css-dropdowns" tabindex="1" >
                                            <option value="">Search by Mileage</option>
                                            <option>0</option>
                                                @for ($i = 10000; $i <= 100000; $i=$i + 10000)
                                                    <option>&lt; {{$i}}</option>
                                                @endfor
                                            <option>&lt; 120,000</option>
                                            <option>&lt; 150,000</option>
                                        </select>
                                    </div>
                                    <div class="my-dropdown min-years-dropdown max-dropdown">
                                        <select name="transmission" class="css-dropdowns" tabindex="1" >
                                            <option value="">Search by Transmission</option>
                                            <option>Automatic</option>
                                            <option>Manual</option>
                                        </select>
                                    </div>
                                    <div class="my-dropdown min-years-dropdown max-dropdown">
                                        <select name="fuel_economy" class="css-dropdowns" tabindex="1" >
                                            <option value="">Search by Fuel Economy</option>
                                            <option>10-20 MPG</option>
                                            <option>20-30 MPG</option>
                                            <option>30-40 MPG</option>
                                            <option>40-50 MPG</option>
                                            <option>50-60 MPG</option>
                                        </select>
                                    </div>
                                    <div class="my-dropdown min-years-dropdown max-dropdown">
                                        <select name="condition" class="css-dropdowns" tabindex="1" >
                                            <option value="">Search by Condition</option>
                                            <option>New</option>
                                            <option>Used</option>
                                        </select>
                                    </div>
                                    <div class="my-dropdown min-years-dropdown max-dropdown">
                                        <select name="location" class="css-dropdowns" tabindex="1" >
                                            <option value="">Search by Location</option>
                                            <option>Toronto</option>
                                            <option>New York</option>
                                            <option>Los Angeles</option>
                                            <option>Vancouver</option>
                                        </select>
                                    </div>
                                    <div class="my-dropdown min-years-dropdown max-dropdown">
                                        <select name="price" class="css-dropdowns" tabindex="1" >
                                            <option value="">Search by Price</option>
                                            <option>&lt; $1,000</option>
                                                @for ($i = 10000; $i <= 100000; $i=$i + 10000)
                                                    <option>&lt; {{'$'}}{{$i}}</option>
                                                @endfor
                                        </select>
                                    </div>
                                    <input type="reset" value="Reset Search Filters" class="pull-left btn-inventory margin-bottom-none md-button" />
                                </form>
                                <div class="side-content">
                                    <div class="list col-lg-12 col-md-12 col-sm-3 padding-bottom-50">
                                        <h3>YEAR</h3>
                                        <ul>
                                                @for ($i = $date_now; $i >= $date_before + 10; $i--)
                                                    <li><a href="#"><span>{{$i}}<strong>(28)</strong></span></a></li>
                                                @endfor
                                            <li><a href="#">View More...</a></li>
                                        </ul>
                                    </div>
                                    <div class="list col-lg-12 col-md-12 col-sm-3 padding-bottom-50">
                                        <h3 class="margin-bottom-25">MAKE</h3>
                                        <ul>
                                            <li><span>Acura <strong>(2)</strong></span></li>
                                            <li><span>BMW <strong>(4)</strong></span></li>
                                            <li><span>Buick <strong>(1)</strong></span></li>
                                            <li><span>Cadillac <strong>(6)</strong></span></li>
                                            <li><span>Chevrolet <strong>(19)</strong></span></li>
                                            <li><span>Chrysler <strong>(7)</strong></span></li>
                                            <li><span>Dodge <strong>(14)</strong></span></li>
                                            <li><span>Ford <strong>(15)</strong></span></li>
                                            <li><span>GMC <strong>(9)</strong></span></li>
                                            <li><span>Hummer <strong>(3)</strong></span></li>
                                            <li><a href="#">View More...</a></li>
                                        </ul>
                                    </div>
                                    <div class="financing_calculator col-lg-12 col-md-12 col-sm-6">
                                        <h3 class="margin-bottom-25">FINANCING CALCULATOR</h3>
                                        <div class="table-responsive">
                                            <table class="table no-border no-margin">
                                                <tbody>
                                                    <tr>
                                                        <td>Cost of Vehicle ($):</td>
                                                        <td><input type="text"  class="number cost" placeholder="10000" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Down Payment ($):</td>
                                                        <td><input type="text"  class="number down_payment" placeholder="1000" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Annual Interest Rate (%):</td>
                                                        <td><input type="text"  class="number interest" placeholder="7" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Term of Loan in Years:</td>
                                                        <td><input type="text"  class="number loan_years" placeholder="5" /></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="bi_weekly clearfix">
                                            <div class="pull-left">Frequency of Payments:</div>
                                            <div class="dropdown_container">
                                                <select class="frequency css-dropdowns">
                                                    <option value='0'>Bi-Weekly</option>
                                                    <option value='1'>Weekly</option>
                                                    <option value='2'>Monthly</option>
                                                </select>
                                            </div>
                                        </div>
                                        <a class="btn-inventory pull-right calculate">Calculate My Payment</a>
                                        <div class="clear"></div>
                                        <div class="table-responsive">
                                            <table>
                                                <tr>
                                                    <td><strong>NUMBER OF PAYMENTS:</strong></td>
                                                    <td><strong>60</strong></td>
                                                </tr>
                                                <tr>
                                                    <td><strong>PAYMENT AMOUNT:</strong></td>
                                                    <td><strong>$ 89.11</strong></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!--container ends--> 
</section>
<!--content ends-->
<div class="clearfix"></div>
<div id="youtube_video">
    <iframe width="560" height="315" src="#" allowfullscreen style="width: 560px; height: 315px; border: 0;"></iframe>
</div>
@endsection