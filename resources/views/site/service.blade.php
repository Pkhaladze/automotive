@extends('layouts.site_master')
@section('content')
<section id="secondary-banner" class="dynamic-image-12"><!--for other images just change the class name of this section block like, class="dynamic-image-2" and add css for the changed class-->
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                <h2>Services</h2>
                <h4>There are no traffic jams along the extra mile</h4>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 ">
                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li>Services</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--secondary-banner ends-->
<div class="message-shadow"></div>
<div class="clearfix"></div>
<section class="content">
    <div class="container">
        <div class="inner-page services">
            <div class="row">
                <div class="consider padding-bottom-40 clearfix">
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 padding-left-none xs-padding-bottom-40">
                        <h3>WHAT CAN WE DO FOR YOU?</h3>
                        <div class="left-content padding-top-20">
                            <p><span class="firstcharacter">R</span>obem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.
                                Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis,
                                ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
                            <p>Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec,
                                pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec,
                                vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 things_to_consider padding-right-none xs-padding-left-none">
                        <h3>THINGS TO CONSIDER</h3>
                        <div class="right-content">
                            <ul>
                                <li>Fully responsive and ready for all mobile devices</li>
                                <li>Integrated inventory management system</li>
                                <li>Simple option panel and very easy to customize</li>
                                <li>Search engine optimization (SEO) is 100% built-in</li>
                                <li>Revolution Slider is included for product marketing</li>
                                <li>Tons of shortcodes for quick and easy add-ons</li>
                                <li>Fully backed by our dedicated support team</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="featured-service clearfix padding-bottom-40 margin-top-30">
                    <h2 class="margin-top-none">Highlight Your <strong>Featured Services</strong></h2>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 padding-bottom-none scroll_effect fadeInUp">
                            <div class="featured">
                                <h5>Mobile Enhanced</h5>
                                <img src="images/android-icon.png" alt="" data-hoverimg="images/android-icon-hover.png">
                                <p>Sed ut perspiciatis unde om
                                    natus error sit volup atem
                                    aperiam, eaque ipsa quae</p>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 padding-bottom-none scroll_effect fadeInUp" data-wow-delay=".2s">
                            <div class="featured">
                                <h5>Platform Tested</h5>
                                <img src="images/icon-windows-8.png" alt="" data-hoverimg="images/icon-windows-8-hover.png">
                                <p>Sed ut perspiciatis unde om
                                    natus error sit volup atem
                                    aperiam, eaque ipsa quae</p>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 padding-bottom-none scroll_effect fadeInUp" data-wow-delay=".4s">
                            <div class="featured">
                                <h5>Social Ready</h5>
                                <img src="images/twitter-icon.png" alt="" data-hoverimg="images/twitter-icon-hover.png">
                                <p>Sed ut perspiciatis unde om
                                    natus error sit volup atem
                                    aperiam, eaque ipsa quae</p>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 padding-bottom-none scroll_effect fadeInUp" data-wow-delay=".6s">
                            <div class="featured">
                                <h5>Video Integration</h5>
                                <img src="images/you-tube.png" alt="" data-hoverimg="images/you-tube-hover.png">
                                <p>Sed ut perspiciatis unde om
                                    natus error sit volup atem
                                    aperiam, eaque ipsa quae</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
            
            <div class="row parallax_parent margin-top-30 padding-bottom-40">
                <div class="parallax_scroll clearfix" data-velocity="-.3" data-fit="300" data-image="images/parallax3.jpg">
                    <div class="overlay" style="background-color: rgba(255, 255, 255, .65); color: #2D2D2D;">
                        <div class="container padding-vertical-10">
                        
                            <h1>Dealership Statistics</h1>
                            
                            <div class="row">
                                
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 margin-vertical-60 xs-margin-vertical-20">
                                    <i class="fa fa-car"></i>
                                    
                                    <span class="animate_number margin-vertical-15">
                                        <span class="number">2,000</span>
                                    </span>
                                    
                                    Cars Sold
                            </div>
                                
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 margin-vertical-60 xs-margin-vertical-20">
                                    <i class="fa fa-shopping-cart"></i>
                                    
                                    <span class="animate_number margin-vertical-15">
                                        $<span class="number">750,000</span>
                                    </span>
                                    
                                    Amount Sold
                                </div>
                                
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 margin-vertical-60 xs-margin-vertical-20">
                                    <i class="fa fa-users"></i>
                                    
                                    <span class="animate_number margin-vertical-15">
                                        <span class="number">100</span>%
                                    </span>
                                    
                                    Customer Satisfaction
                                </div>
                                
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 margin-vertical-60 xs-margin-vertical-20">
                                    <i class="fa fa-tint"></i>
                                    
                                    <span class="animate_number margin-vertical-15">
                                        <span class="number">3,600</span>
                                    </span>
                                    
                                    Oil Changes
                                </div>
                                
                        </div>
                    </div>
                </div>
            </div>
            </div>
            
            <div class="row">
                <div class="detail-service clearfix margin-top-30 padding-bottom-40">
                    <h2 class="margin-top-none">Easily Layout Your <strong>Detailed Services</strong></h2>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 scroll_effect fadeInLeft">
                        <div class="details margin-top-25">
                            <h5 class="customize">Highly Customizable</h5>
                            <p class="padding-top-10">Sociis natoque penatibus et magnis dis parturient etah
                                montes, nascetur ridiculus mus. Donec quam felis, A
                                ultricies nec, pellentesque eu, pretium quis, sem. Cum 
                                sociis natoque penatibus et magnis dis parturient nas. </p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 scroll_effect fadeInUp">
                        <div class="details margin-top-25">
                            <h5 class="awards">Award Winning</h5>
                            <p class="padding-top-10">Sociis natoque penatibus et magnis dis parturient etah
                                montes, nascetur ridiculus mus. Donec quam felis, A
                                ultricies nec, pellentesque eu, pretium quis, sem. Cum 
                                sociis natoque penatibus et magnis dis parturient nas. </p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 scroll_effect fadeInRight">
                        <div class="details margin-top-25">
                            <h5 class="music">Music To Your Ears</h5>
                            <p class="padding-top-10">Sociis natoque penatibus et magnis dis parturient etah
                                montes, nascetur ridiculus mus. Donec quam felis, A
                                ultricies nec, pellentesque eu, pretium quis, sem. Cum 
                                sociis natoque penatibus et magnis dis parturient nas. </p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 scroll_effect fadeInLeft" data-wow-delay=".2s">
                        <div class="details margin-top-25">
                            <h5 class="work">Easy To Work With</h5>
                            <p class="padding-top-10 margin-bottom-none">Sociis natoque penatibus et magnis dis parturient etah
                                montes, nascetur ridiculus mus. Donec quam felis, A
                                ultricies nec, pellentesque eu, pretium quis, sem. Cum 
                                sociis natoque penatibus et magnis dis parturient nas. </p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 scroll_effect fadeInUp" data-wow-delay=".2s">
                        <div class="details margin-top-25">
                            <h5 class="ultra">Ultra Responsive</h5>
                            <p class="padding-top-10 margin-bottom-none">Sociis natoque penatibus et magnis dis parturient etah
                                montes, nascetur ridiculus mus. Donec quam felis, A
                                ultricies nec, pellentesque eu, pretium quis, sem. Cum 
                                sociis natoque penatibus et magnis dis parturient nas. </p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 scroll_effect fadeInRight" data-wow-delay=".2s">
                        <div class="details margin-top-25">
                            <h5 class="flexible">Flexible Framework</h5>
                            <p class="padding-top-10 margin-bottom-none">Sociis natoque penatibus et magnis dis parturient etah
                                montes, nascetur ridiculus mus. Donec quam felis, A
                                ultricies nec, pellentesque eu, pretium quis, sem. Cum 
                                sociis natoque penatibus et magnis dis parturient nas. </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 featured-brand-slider padding-left-none padding-right-none margin-top-30">
                    <div class="featured-brand">
                        <h3 class="margin-bottom-20">SOME OF OUR FEATURED BRANDS</h3>
                        <div class="arrow2" id="slideControls"><span class="prev-btn"></span><span class="next-btn"></span></div>
                        <div class="carasouel-slider featured_slider">
                            <div class="slide jquery scroll_effect fadeInUp"><a href="#">jquery</a></div>
                            <div class="slide html5 scroll_effect fadeInUp" data-wow-delay=".1s"><a href="#">html5</a></div>
                            <div class="slide my-sql scroll_effect fadeInUp" data-wow-delay=".2s"><a href="#">my-sql</a></div>
                            <div class="slide javascript scroll_effect fadeInUp" data-wow-delay=".3s"><a href="#">javascript</a></div>
                            <div class="slide wordpress scroll_effect fadeInUp" data-wow-delay=".4s"><a href="#">wordpress</a></div>
                            <div class="slide php scroll_effect fadeInUp" data-wow-delay=".5s"><a href="#">php</a></div>
                            <div class="slide jquery"><a href="#">jquery</a></div>
                            <div class="slide html5"><a href="#">html5</a></div>
                            <div class="slide my-sql"><a href="#">my-sql</a></div>
                            <div class="slide javascript"><a href="#">javascript</a></div>
                            <div class="slide wordpress"><a href="#">wordpress</a></div>
                            <div class="slide php"><a href="#">php</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--container ends--> 
</section>
<!--content ends--> 
@endsection