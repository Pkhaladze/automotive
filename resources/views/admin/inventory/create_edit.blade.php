@extends('layouts.admin_master')
@section('content')
@php
//dd($detail);
/*    $selector = [];
    if ($detail) {
        $selector['brand'] = $detail->item_brend->name;
        $selector['brand'] = $detail->item_brend->name;
        dd($selector['brand']);
    } else {

    }*/
    
@endphp
{{-- 
   @if (session()->has('success'))
        <div class="col-md-offset-2 col-md-8" >
              <div class="alert alert-success">
                <strong>{{ session('success') }}</strong>
              </div>
        </div>
    @endif
    @if (isset($errors) && count($errors) > 0)
        <ul class="list-group">
            @foreach ($errors->all() as $error)
                <li class="list-group-item list-group-item-danger">
                    <strong>{{ $error }}</strong>
                </li>
            @endforeach
        </ul>
    @endif
    @php
        if (isset($item)) {
            $edit = $item;
            $edit->link = $item->id;
        }
        else { 
            $edit = new item;
            $edit->link = "";
        }
    @endphp
 --}}


<div class="container left-content">
    <form method="POST" enctype="multipart/form-data" action="{{url('/admin/inventory')}}">
        {{ csrf_field() }}
        <div class="form-horizontal col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <fieldset>
                <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <label class="col-md-4 control-label">Brand</label>  
                    <div class="col-md-6">
                       <select class="form-control" id="brend_select" name="item_brend_id">
                            <option></option>
                                @foreach ($brands as $brand)
                                @if (isset($detail))
                                    <option {{$brand->id == $detail->item_brend_id ? "selected='selected'":"" }}
                                    value="{{$brand->id}}">{{$brand->name}}</option>
                                @else
                                    <option value="{{$brand->id}}">{{$brand->name}}</option>
                                 @endif
                                @endforeach
                       </select>
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <label class="col-md-4 control-label">Model</label>  
                    <div class="col-md-6">
                       <select class="form-control" id="brend_select" name="item_model_id">
                            <option></option>
                                @foreach ($models as $model)
                                    @if (isset($detail))
                                        <option {{$model->id == $detail->item_model_id ? "selected='selected'":"" }} value="{{$model->id}}">{{$model->name}}</option>
                                    @else
                                        <option value="{{$model->id}}">{{$model->name}}</option>
                                     @endif
                                @endforeach
                       </select>
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <label class="col-md-4 control-label">Body Style</label>  
                    <div class="col-md-6">
                       <select class="form-control" id="brend_select" name="item_body_style_id">
                            <option></option>
                            @foreach ($body_styles as $body_style)
                                @if (isset($detail))
                                    <option {{$body_style->id == $detail->item_body_style_id ? "selected='selected'":"" }} value="{{$body_style->id}}">{{$body_style->name}}</option>
                                @else
                                    <option value="{{$body_style->id}}">{{$body_style->name}}</option>
                                @endif
                            @endforeach
                       </select>
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <label class="col-md-4 control-label">condition</label>  
                    <div class="col-md-6">
                       <select class="form-control" id="brend_select" name="condition">
                            <option></option>
                            @if (isset($detail))
                            <option {{$detail->condition == 'used' ? "selected='selected'":""}} value="{{$detail->condition}}"> used </option>
                            <option {{$detail->condition == 'new' ? "selected='selected'":""}} value="{{$detail->condition}}"> new </option>
                            @else
                                <option value="new">New</option>
                                <option value="used" selected="selected">Used</option>
                            @endif
                       </select>
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <label class="col-md-4 control-label">Price</label>  
                    <div class="col-md-6">
                        <input  name="price" value="{{isset($detail) ? $detail->price : ""}}" type="text" placeholder="Price" class="form-control input-md">  
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <label class="col-md-4 control-label">Location</label>  
                    <div class="col-md-6">
                        <input  name="location" value="{{isset($detail) ? $detail->location : ""}}" type="text" placeholder="Location" class="form-control input-md">  
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <label class="col-md-4 control-label">Year</label>  
                    <div class="col-md-6">
                        <input  name="year" value="{{isset($detail) ? $detail->year : ""}}" type="text" placeholder="Year" class="form-control input-md datetimepicker10">  
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label class="col-md-2 control-label">Vehicle Overview</label>  
                    <div class="col-md-10">
                        <textarea name="overview" value="" type="text" placeholder="Vehicle Overview" class="form-control" rows="3">{{isset($detail) ? $detail->overview : ""}}</textarea>
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label class="col-md-2 control-label">Vehicle Comment</label>  
                    <div class="col-md-10">
                        <textarea name="comment" value="" type="text" placeholder="Vehicle Comment" class="form-control" rows="3">{{isset($detail) ? $detail->comment : ""}}</textarea>
                    </div>
                </div>
                <div class="clearfix"></div>
                <hr>

                <table style="width: 100%;">
                    <tr><td colspan="2"><h4>Features & Options</h4></td></tr>
                    <tr class="features_label">
                        <td class="features_checkbox">
                            @if (isset($detail))
                                @foreach ($feature_detail as $checkbox)
                                    <input type="checkbox" {{$checkbox->status == 1 ? "checked" : ""}} value="1" name="{{ $checkbox->item_feature->id }}" id="{{ $checkbox->item_feature->id }}">
                                    <label class="checkbox-inline" for="{{ $checkbox->item_feature->id }}">{{ $checkbox->item_feature->name }}</label>                               
                                @endforeach
                            @else
                                @foreach ($features as $feature)
                                    <input type="checkbox" value="1" name="{{ $feature->id }}" id="{{ $feature->id }}">
                                    <label class="checkbox-inline" for="{{ $feature->id }}">{{ $feature->name }}</label>
                                @endforeach
                            @endif
                        </td>
                    </tr>
                </table>
                <hr>

                <table style="width: 100%;">
                <tr><td colspan="2"><h4>Technical Specifications</h4></td></tr>
                    <tbody>
                        @if (isset($detail))
                            @foreach ($technical_detail as $value)
                                <tr>
                                    <td>{{$value->item_technical->name}}</td>
                                    <td>
                                        <input  name="technical_{{$value->item_technical_id}}" value="{{$value->value}}" type="text" placeholder="{{$value->item_technical->name}}" class="form-control input-md"></td>
                                </tr>
                            @endforeach
                        @else
                            @foreach ($technicals as $technical)
                            {{-- {{dd($technical)}} --}}
                                <tr>
                                    {{-- <td>{{$element->item_technical}}</td> --}}
                                    <td>{{$technical->name}}</td>
                                    <td><input  name="technical_{{$technical->id}}" value="" type="text" placeholder="{{$technical->name}}" class="form-control input-md"></td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
                <hr>
                <div id="photo">
                    <div class="form-group col-xs-11">
                        <label class="col-xs-12 col-sm-6 col-md-6 col-lg-2">Photo-1</label>
                        <input class="col-xs-12 col-sm-6 col-md-6 col-lg-10" type="file" name="photo[]">
                    </div>
                </div>
                    <p type="button" class="btn btn-primary col-xs-1" id="someID"> + </p>
                <div class="clearfix"></div>

                <hr>
                <!-- Button -->
                <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    {{-- <label class="col-sm-8 col-md-8 col-lg-8 control-label" for="add"></label> --}}
                        <button id="add" name="add"  style="float: right; width: 150px" class="btn btn-primary">Add</button>
                        <div class="clearfix"></div>

                </div>
            </fieldset>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(function () {
        $('.datetimepicker10').datetimepicker({
            format: 'YYYY/MM/DD'
        });
    });
</script>
<script type="text/javascript">
        $i = 2;
        $('#someID').click(function(){
            $p =  '<div class="form-group col-xs-11"><label class="col-xs-12 col-sm-6 col-md-6 col-lg-2">Photo-' + $i + '</label><input class="col-xs-12 col-sm-6 col-md-6 col-lg-10" type="file" name="photo[]"></div>';
            $i +=1;
            document.getElementById("photo").innerHTML += $p;
        });

</script>
@endsection
