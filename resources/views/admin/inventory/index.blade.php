@php
foreach ($details as $detail) {
//dd($detail);
    # code...
}
@endphp
@extends('layouts.admin_master')
@section('content')
<div class="left-content">
    <table class="table table-striped table-hover"> 
            <thead> 
                <tr> 
                    <th>ID</th>
                    <th>Brand</th> 
                    <th>Model</th> 
                    <th>condition</th>
                    <th>Year</th>
                    <th>Price</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($details as $detail)
                    <tr> 
                    <td scope="row">{{$detail->id}}</td>  
                    <td>{{$detail->item_brend->name}}</td>
                    <td>{{$detail->item_model->name}}</td>
                    <td>{{$detail->condition}}</td>
                    <td>{{$detail->year}}</td>
                    <td>{{$detail->price}}$</td>
                    <td>
                        <a href="{{url('admin/inventory/' . $detail->id . '/edit')}}">
                            <i class="glyphicon glyphicon-edit"></i>
                        </a>
                    </td>
                    <td>
                        <a href="{{url('admin/projects/1/edit')}}">
                            <i class="glyphicon glyphicon-remove"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
</div>
            

@endsection