<!--/content-inner-->
<div class="left-content">
   <div class="mother-grid-inner">
        <div class="header-main">
            <div class="profile_details w3l">       
                <ul>
                    <li class="dropdown profile_details_drop">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                            <div class="profile_img">   
                                {{-- <span class="prfil-img"><img src="adminpage/images/in4.jpg" alt=""> </span> --}} 
                                <div class="user-name">
                                    <span>Administrator</span>
                                </div>
                                <i class="fa fa-angle-down"></i>
                                <i class="fa fa-angle-up"></i>
                                <div class="clearfix"></div>    
                            </div>  
                        </a>
                        <ul class="dropdown-menu drp-mnu">
                            <li> 
                                <a href="{{ asset('/Logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                    <i class="fa fa-sign-out"></i> Logout</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div> 
            <div class="clearfix"> </div>  
        </div>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a target="_blank" href="{{ asset('/') }}">Home</a> <i class="fa fa-angle-right"></i></li>
        </ol>
        <script>
        $(document).ready(function() {
             var navoffeset=$(".header-main").offset().top;
             $(window).scroll(function(){
                var scrollpos=$(window).scrollTop(); 
                if(scrollpos >=navoffeset){
                    $(".header-main").addClass("fixed");
                }else{
                    $(".header-main").removeClass("fixed");
                }
             });
             
        });
        </script>
        <div class="inner-block"></div>
    </div>
</div>

