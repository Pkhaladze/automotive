
<!--/sidebar-menu-->
<div class="sidebar-menu">
    <header class="logo1">
        <a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span> </a> 
    </header>
    <div style="border-top:1px ridge rgba(255, 255, 255, 0.15)"></div>
    <div class="menu">
        <ul id="menu" >
            <li><a href="{{ asset('/admin') }}"><i class="fa fa-tachometer"></i> <span>Dashboard</span><div class="clearfix"></div></a></li>
            <li id="menu-academico" ><a href="{{ asset('admin/inventory') }}"><i class="fa fa-list-ul" aria-hidden="true"></i><span>Inventory</span> <span class="fa fa-angle-right" style="float: right"></span><div class="clearfix"></div></a>
               <ul id="menu-academico-sub" >
                    <li id="menu-academico-avaliacoes" ><a href="{{ asset('admin/inventory') }}">Tables</a></li>
                    <li id="menu-academico-avaliacoes" ><a href="{{ asset('admin/inventory/create') }}">Insert</a></li>
              </ul>
            </li>
        </ul>
    </div>
</div>
<div class="clearfix"></div>