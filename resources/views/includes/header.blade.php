<header  data-spy="affix" data-offset-top="1" class="clearfix">
    <section class="toolbar">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 left_bar">

                </div>
                <div class="col-lg-6">
                    <ul class="right-none pull-right company_info">
                        <li><a href="tel:18005670123"><i class="fa fa-phone"></i> 1-000-000-0000</a></li>
                        <li class="address"><a href="{{ asset('/contact') }}"><i class="fa fa-map-marker"></i>5711 Industry Park Dr San Antonio TX 78218</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="toolbar_shadow"></div>
    </section>
    <div class="bottom-header" >
        <div class="container">
            <nav class="navbar navbar-default" role="navigation">
                <div class="container-fluid"> 
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                        <a class="navbar-brand" href="{{ asset('/') }}"><span class="logo"><span class="primary_text brend_logo">ANESA autos</span> <span class="secondary_text"></span></span></a>
                    </div>
                    @php
                        if (isset($active_link)) {
                            
                        } 
                        else {
                            $active_link = 'home';
                        }
                    @endphp
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav pull-right">
                            <li class="{{$active_link == 'home' ? "active":"" }}"><a href="{{ asset('/') }}">Home</a></li>
                            <li class="{{--dropdown--}} {{$active_link == 'inventory' ? "active":"" }}"><a href="{{ asset('/inventory') }}" {{--class="dropdown-toggle"--}}>Inventory </a>
                                {{-- <ul class="dropdown-menu">
                                    <li><a href="{{ asset('/trade') }}">Trade</a></li>
                                </ul> --}}
                            </li>
                            <li class="{{$active_link == 'service' ? "active":"" }}"><a href="{{ asset('/service') }}">Services</a></li>
                            <li class="{{$active_link == 'our_team' ? "active":"" }}"><a href="{{ asset('/our_team') }}">Our Team</a></li>
                            <li class="{{$active_link == 'portfolio' ? "active":"" }}"><a href="{{ asset('/portfolio') }}">Portfolio</a></li>
                            <li class="{{$active_link == 'trade' ? "active":"" }}"><a href="{{ asset('/trade') }}">Trade</a></li>
                            <li class="{{$active_link == 'about' ? "active":"" }}"><a href="{{ asset('/about') }}">About</a></li>
                            <li class="{{$active_link == 'contact' ? "active":"" }}"><a href="{{ asset('/contact') }}">Contact</a></li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse --> 
                </div>
                <!-- /.container-fluid --> 
            </nav>
        </div>
        <div class="header_shadow"></div>
    </div>
</header>
<div class="clearfix"></div>