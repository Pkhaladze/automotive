


<!--Footer Start-->
<footer>
    <div class="container">
        <div class="row">
            <div class="car-rate-block clearfix margin-top-30 padding-bottom-40">
                <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 xs-margin-bottom-40 sm-margin-bottom-none padding-left-none scroll_effect bounceInLeft">
                    <div class="small-block clearfix">
                        <h4 class="margin-bottom-25 margin-top-none">Financing.</h4>
                        <a href="javascript:void(0)"><span class="align-center"><i class="fa fa-tag fa-7x"></i></span></a> 
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 xs-margin-bottom-40 sm-margin-bottom-none scroll_effect bounceInLeft" data-wow-delay=".2s">
                    <div class="small-block clearfix">
                        <h4 class="margin-bottom-25 margin-top-none">Warranty.</h4>
                        <a href="javascript:void(0)"><span class="align-center"><i class="fa fa-cogs fa-7x"></i></span></a> 
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 padding-left-none padding-right-none hours_operation">
                    <div class="small-block clearfix">
                        <h4 class="margin-bottom-25 margin-top-none">What are our Hours of Operation?</h4>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 xs-margin-bottom-40 sm-margin-bottom-40 md-margin-bottom-none scroll_effect bounceInUp" data-wow-delay=".4s">
                                <table class="table table-bordered no-border font-13px margin-bottom-none">
                                    <thead>
                                        <tr>
                                            <td colspan="2"><strong>Sales Department</strong></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Mon:</td>
                                            <td>8:00am - 5:00pm</td>
                                        </tr>
                                        <tr>
                                            <td>Tue:</td>
                                            <td>8:00am - 9:00pm</td>
                                        </tr>
                                        <tr>
                                            <td>Wed:</td>
                                            <td>8:00am - 5:00pm</td>
                                        </tr>
                                        <tr>
                                            <td>Thu:</td>
                                            <td>8:00am - 9:00pm</td>
                                        </tr>
                                        <tr>
                                            <td>Fri:</td>
                                            <td>8:00am - 6:00pm</td>
                                        </tr>
                                        <tr>
                                            <td>Sat:</td>
                                            <td>9:00am - 5:00pm</td>
                                        </tr>
                                        <tr>
                                            <td>Sun:</td>
                                            <td>Closed</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 xs-margin-bottom-40 sm-margin-bottom-none scroll_effect bounceInUp" data-wow-delay=".4s">
                                <table class="table table-bordered no-border font-13px margin-bottom-none">
                                    <thead>
                                        <tr>
                                            <td colspan="2"><strong>Service Department</strong></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Mon:</td>
                                            <td>8:00am - 5:00pm</td>
                                        </tr>
                                        <tr>
                                            <td>Tue:</td>
                                            <td>8:00am - 9:00pm</td>
                                        </tr>
                                        <tr>
                                            <td>Wed:</td>
                                            <td>8:00am - 5:00pm</td>
                                        </tr>
                                        <tr>
                                            <td>Thu:</td>
                                            <td>8:00am - 9:00pm</td>
                                        </tr>
                                        <tr>
                                            <td>Fri:</td>
                                            <td>8:00am - 6:00pm</td>
                                        </tr>
                                        <tr>
                                            <td>Sat:</td>
                                            <td>9:00am - 5:00pm</td>
                                        </tr>
                                        <tr>
                                            <td>Sun:</td>
                                            <td>Closed</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 xs-margin-bottom-40 sm-margin-bottom-none scroll_effect bounceInRight" data-wow-delay=".2s">
                    <div class="small-block clearfix">
                        <h4 class="margin-bottom-25 margin-top-none">About Us.</h4>
                        <a href="{{ asset('/about') }}"><span class="align-center"><i class="fa fa-users fa-7x"></i></span></a> 
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 xs-margin-bottom-40 sm-margin-bottom-none padding-right-none scroll_effect bounceInRight">
                    <div class="small-block clearfix">
                        <h4 class="margin-bottom-25 margin-top-none">Find Us.</h4>
                        <a href="{{ asset('/contact') }}"><span class="align-center"><i class="fa fa-map-marker fa-7x"></i></span></a> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<div class="clearfix"></div>
<section class="copyright-wrap padding-bottom-10">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="logo-footer margin-bottom-20 md-margin-bottom-20 sm-margin-bottom-10 xs-margin-bottom-20"><a href="{{ asset('/') }}">
                    <h1>ANESA autos</h1>
                    <span></span></a>
                </div>
                <p>Copyright &copy; {{ date('Y') }} All rights reserved</p>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                <ul class="social margin-bottom-25 md-margin-bottom-25 sm-margin-bottom-20 xs-margin-bottom-20 xs-padding-top-10 clearfix">
                    <li><a class="sc-1" href="javascript:void(0)"></a></li>
                    <li><a class="sc-2" href="javascript:void(0)"></a></li>
                    <li><a class="sc-3" href="javascript:void(0)"></a></li>
                    <li><a class="sc-4" href="javascript:void(0)"></a></li>
                    <li><a class="sc-5" href="javascript:void(0)"></a></li>
                    <li><a class="sc-6" href="javascript:void(0)"></a></li>
                    <li><a class="sc-7" href="javascript:void(0)"></a></li>
                    <li><a class="sc-8" href="javascript:void(0)"></a></li>
                    <li><a class="sc-9" href="javascript:void(0)"></a></li>
                    <li><a class="sc-10" href="javascript:void(0)"></a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<div class="back_to_top">
    <img src="{{ asset('images/arrow-up.png') }}" alt="scroll up" />
</div>
    <script src="{{ asset('js/jquery.mixitup.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="https://www.google.com/recaptcha/api.js?ver=4.1"></script>
    <script src="{{ asset('js/retina.js') }}"></script> 
    <script type="text/javascript" src="{{ asset('js/jquery.parallax.js') }}"></script> 
    <script type="text/javascript" src="{{ asset('js/jquery.inview.min.js') }}"></script> 
    <script src="{{ asset('js/main.js') }}"></script> 
    <script type="text/javascript" src="{{ asset('js/jquery.fancybox.js') }}"></script> 
    <script src="{{ asset('js/modernizr.custom.js') }}"></script>
    <script src="{{ asset('js/social-likes.min.js') }}"></script>
    <script defer src="{{ asset('js/jquery.flexslider.js') }}"></script> 
    <script src="{{ asset('js/jquery.bxslider.js') }}" type="text/javascript"></script> 
    <script src="{{ asset('js/jquery.selectbox-0.2.js') }}" type="text/javascript"></script> 
    <script type="text/javascript" src="{{ asset('js/jquery.mousewheel.js') }}"></script> 
    <script type="text/javascript" src="{{ asset('js/jquery.easing.js') }}"></script>